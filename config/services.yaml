# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: fr_FR
    extranet.log.email: '%env(resolve:EXTRANET_LOG_EMAIL)%'
    extranet.log.pwd: '%env(resolve:EXTRANET_LOG_PWD)%'
    extranet.route.auth: '%env(resolve:EXTRANET_ROUTE_AUHTHENTIFICATION)%'
    extranet.route.add_commande: '%env(resolve:EXTRANET_ROUTE_ADD_COMMANDE)%'
    extranet.route.commande_shipped: '%env(resolve:EXTRANET_ROUTE_COMMANDE_SHIPPED)%'
    extranet.route.end_sale: '%env(resolve:EXTRANET_ROUTE_END_SALE)%'
    app.temp_image.dir: '%env(resolve:LIIP_PUBLIC_DIR)%/media/image/temp'
    ecom.base_url: '%env(resolve:ECOM_BASE_URL)%'
    ecom.default.channel_context: '%env(resolve:ECOM_DEFAULT_CHANNEL_CONTEXT)%'
    ecom.version: '%env(resolve:APP_VERSION)%'

services:
    # Default configuration for services in *this* file
    _defaults:
        # Automatically injects dependencies in your services
        autowire: true

        # Automatically registers your services as commands, event subscribers, etc.
        autoconfigure: true

        # Allows optimizing the container by removing unused services; this also means
        # fetching services directly from the container via $container->get() won't work
        public: false

    _instanceof:
        Sylius\Bundle\ResourceBundle\Controller\ResourceController:
            autowire: false
        Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType:
            autowire: false

    # Makes classes in src/ available to be used as services;
    # this creates a service per class whose id is the fully-qualified class name
    App\:
        resource: '../src/*'
        exclude: '../src/{Entity,Fixture,Migrations,Tests,Kernel.php}'

    # Controllers are imported separately to make sure services can be injected
    # as action arguments even if you don't extend any base controller class
    App\Controller\:
        resource: '../src/Controller'
        tags: ['controller.service_arguments']

# extension de resourceController pour bulk transfert
    App\Controller\ProductController:
        autowire: false
        public: true

# extension de resourceController pour cloture channel
    App\Controller\ChannelController:
        autowire: false
        public: true

# extension de OrderItemController pour stock par canal
    App\Controller\Sylius\Bundle\OrderBundle\OrderItemController:
        autowire: false
        public: true

# HomepageController
    sylius.controller.shop.homepage:
        class: App\Controller\Sylius\Bundle\ShopBundle\HomepageController
        autowire: true
        tags: ['controller.service_arguments']

# OrderController pour vérification paiement différé
    App\Controller\Sylius\Bundle\OrderBundle\OrderController:
        autowire: false
        public: true

# ProvinceController pour suppression champ Province dans checkout adresse
    App\Controller\Sylius\Bundle\AddressingBundle\ProvinceController:
        autowire: false
        public: true

# Gestion des menus
    App\Service\AdminMenu:
        bind:
            $adminItemsAuth: '%kernel.project_dir%/src/Service/Config/admin_menu.yaml'

    App\EventListener\AdminMenuListener:
        tags: [
            { name: 'kernel.event_listener', event: 'sylius.menu.admin.main', method: 'filterAdminMenuItems' }
        ]

    App\EventListener\ProductUpdateMenuListener:
        tags: [
            { name: 'kernel.event_listener', event: 'sylius.menu.admin.product.update', method: 'removeVariantButton' }
        ]

    App\EventListener\ProductFormMenuListener:
        tags: [
            { name: 'kernel.event_listener', event: 'sylius.menu.admin.product.form', method: 'filterAdminFormMenuItems' }
        ]

    App\EventListener\AdminProductGridListener:
        tags:
            - { name: kernel.event_listener, event: sylius.grid.admin_product, method: 'removeAction' } 

    App\EventListener\AdminOrderShowListener:
        tags:
            - { name: kernel.event_listener, event: sylius.menu.admin.order.show, method: 'changeColor' } 

# Listener pour channel image
    app.listener.image_upload:
        class: Sylius\Bundle\CoreBundle\EventListener\ImageUploadListener
        # parent: sylius.listener.images_upload
        autowire: true
        autoconfigure: false
        public: false
        tags:
            - { name: kernel.event_listener, event: sylius.channel.pre_create, method: uploadImage }
            - { name: kernel.event_listener, event: sylius.channel.pre_update, method: uploadImage }

# Listener pour évènement OrderComplete
    app.listener.order_complete:
        class: App\EventListener\OrderCompleteListener
        tags:
            - { name: kernel.event_listener, event: sylius.order.post_complete, method: processEvent, priority: -5 }

# Listener pour évènement order shipped 
    app.listener.shipment:
        class: App\EventListener\ShipmentListener
        tags:
            - { name: kernel.event_listener, event: sylius.shipment.post_ship, method: orderIsShipped, priority: -5 }

# Listener pour évènement sylius.product.pre_update
    app.listener.product:
        class: App\EventListener\ProductListener
        tags:
            - { name: kernel.event_listener, event: sylius.product.pre_update, method: resetIsNew, priority: -5 }

# Listener pour évènementsylius.payment.pre_complete
    # app.admin.availability_checker:
    #     decorates: 'sylius.availability_checker.default'
    #     class: 'App\Sylius\Component\Inventory\Checker\AvailabilityChecker'
    # # App\Sylius\Component\Inventory\Checker\AvailabilityCheckerInterface:
    # #     '@Sylius\Component\Inventory\Checker\AvailabilityChecker'
    # app.listener.payment.pre_complete:
    #     # decorates: 'Sylius\Bundle\CoreBundle\EventListener\PaymentPreCompleteListener'
    #     class: 'App\Sylius\Bundle\CoreBundle\EventListener\PaymentPreCompleteListener'
    #     arguments: 
    #         - '@app.admin.availability_checker'
    #     tags: 
    #         - { name: kernel.event_listener, event: sylius.payment.pre_complete, method: checkStockAvailability, priority: 256 }

        # <service id="Sylius\Bundle\CoreBundle\EventListener\PaymentPreCompleteListener">
        #     <argument type="service" id="Sylius\Component\Inventory\Checker\AvailabilityCheckerInterface" />
        #     <tag name="kernel.event_listener" event="sylius.payment.pre_complete" method="checkStockAvailability" />
        # </service>

# Listener pour channel
    app.listener.channel:
        class: App\EventListener\ChannelListener
        tags: [
            { name: doctrine.orm.entity_listener, event: preUpdate, entity: App\Entity\Channel\Channel },
            { name: doctrine.orm.entity_listener, event: postUpdate, entity: App\Entity\Channel\Channel },
            { name: doctrine.orm.entity_listener, event: prePersist, entity: App\Entity\Channel\Channel },
            { name: doctrine.orm.entity_listener, event: postPersist, entity: App\Entity\Channel\Channel }
        ]

# Listener pour Order
    # app.listener.order:
    #     class: App\EventListener\OrderListener
    #     tags: [
    #         { name: doctrine.orm.entity_listener, event: prePersist, entity: App\Entity\Order\Order }
    #     ]

# Form extension
    app.form.extension.type.product_variant:
        class: App\Form\Extension\ProductVariantTypeExtension
        tags:
            - { name: form.type_extension, extended_type: Sylius\Bundle\ProductBundle\Form\Type\ProductVariantType, priority: -5 }

    # app.form.extension.type.product:
    #     class: App\Form\Extension\ProductTypeExtension
    #     tags:
    #         - { name: form.type_extension, extended_type: Sylius\Bundle\ProductBundle\Form\Type\ProductType, priority: -5 }

    app.form.extension.type.payment:
        class: App\Form\Extension\PaymentTypeExtension
        tags:
            - { name: form.type_extension, extended_type: Sylius\Bundle\CoreBundle\Form\Type\Checkout\PaymentType, priority: -5 }

    app.form.extension.type.checkout_complete:
        class: App\Form\Extension\CompleteTypeExtension
        tags:
            - { name: form.type_extension, extended_type: Sylius\Bundle\CoreBundle\Form\Type\Checkout\CompleteType, priority: -5 }

# extension des fixtures
    sylius.fixture.factory.product:
        class: App\Fixture\Factory\ProductFactory
        arguments:
            - "@sylius.factory.product" 
            - "@sylius.factory.product_variant" 
            - "@sylius.factory.channel_pricing" 
            - "@sylius.generator.product_variant" 
            - "@sylius.factory.product_attribute_value" 
            - "@sylius.factory.product_image" 
            - "@sylius.factory.product_taxon" 
            - "@sylius.image_uploader" 
            - "@sylius.generator.slug" 
            - "@sylius.repository.taxon" 
            - "@sylius.repository.product_attribute" 
            - "@sylius.repository.product_option" 
            - "@sylius.repository.channel" 
            - "@sylius.repository.locale" 
            - "@sylius.repository.tax_category" 
            - "@file_locator" 
        public: true

    sylius.fixture.product:
        class: App\Fixture\ProductFixture
        arguments:
            - "@sylius.manager.product"
            - "@sylius.fixture.factory.product"
        tags:
            - { name: sylius_fixtures.fixture }

    sylius.fixture.factory.channel:
        class: App\Fixture\Factory\ChannelFactory
        arguments:
            - "@sylius.factory.channel" 
            - "@sylius.repository.locale" 
            - "@sylius.repository.currency" 
            - "@sylius.repository.zone" 
            - "@sylius.repository.taxon" 
            - "@sylius.factory.shop_billing_data" 
        public: true

    sylius.fixture.channel:
        class: App\Fixture\ChannelFixture
        arguments:
            - "@sylius.manager.channel"
            - "@sylius.fixture.factory.channel"
        tags:
            - { name: sylius_fixtures.fixture }

    sylius.fixture.factory.shop_user:
        class: App\Fixture\Factory\ShopUserFactory
        arguments:
            - "@sylius.factory.shop_user" 
            - "@sylius.factory.customer" 
            - "@sylius.repository.customer_group" 
        public: true

    sylius.fixture.shop_user:
        class: App\Fixture\ShopUserFixture
        arguments:
            - "@sylius.manager.shop_user"
            - "@sylius.fixture.factory.shop_user"
        tags:
            - { name: sylius_fixtures.fixture }

# Fixture pour les BankInfos
    app.fixture.factory.bank_infos:
        class: App\Fixture\Factory\BankInfosFactory
        arguments:
            - "@app.factory.bank_infos" 
            - "@app.repository.bank_infos" 
        public: true

    app.fixture.bank_infos:
        class: App\Fixture\BankInfosFixture
        arguments:
            - "@app.manager.bank_infos"
            - "@app.fixture.factory.bank_infos"
        tags:
            - { name: sylius_fixtures.fixture }

# ContextProvider pour channel principal 
    App\ContextProvider\MainChannelContextProvider:
        arguments:
            - '@sylius.repository.channel'
        tags:
            - { name: sylius.ui.template_event.context_provider }

# Normalizers
  # Pour les retours OK sur Product vers l'extranet 
    App\Serializer\ExtranetProductNormalizer:
        tags:
            - { name: 'serializer.normalizer', priority: 200 }

  # Pour les retours OK sur Order vers l'extranet 
    App\Serializer\ExtranetOrderNormalizer:
        tags:
            - { name: 'serializer.normalizer', priority: 200 }

  # Pour les retours erreur 400 vers l'extranet 
    App\Serializer\HydraErrorNormalizer:
        decorates: 'Sylius\Bundle\ApiBundle\Serializer\HydraErrorNormalizer'
        arguments:
            - '@App\Serializer\HydraErrorNormalizer.inner'
        bind: 
            $debug: '%kernel.debug%'

  # Pour ne faire apparaître que les paramètres nécessaires dans la doc API Platform
    App\Serializer\ApiPlatformDocumentationNormalizer:
        decorates: 'api_platform.swagger.normalizer.documentation'
        arguments: 
            - '@App\Serializer\ApiPlatformDocumentationNormalizer.inner'
        autoconfigure: false

  # Validation de la quantité demandée par rapport à la quantité disponible
    App\Sylius\Bundle\CoreBundle\Validator\Constraints\CartItemAvailabilityValidator:
        decorates: 'sylius.validator.cart_item_availability'
        autoconfigure: false

# http service pour requêtes vers Extranet
    app.extranet.http:
        class: App\Service\ExtranetHttpService

# service envoi extranet nouvelle commande sur changement d'état paiement'
    app.extranet.send.new_order:
        class: App\Service\ExtranetSendNewOrder
        public: true

# channel context hostname based resolver
    sylius.context.channel.request_based.resolver.hostname_based:
        class: App\Sylius\Component\Channel\Context\RequestBased\HostnameBasedRequestResolver
        public: false
        arguments:
            - "@sylius.repository.channel"
        tags:
            - { name: sylius.context.channel.request_based.resolver, priority: 256 }

    # sylius.context.channel.request_based:
    #     class: App\Sylius\Component\Channel\Context\RequestBased\ChannelContext
    #     public: false
    #     arguments:
    #         - "@sylius.context.channel.request_based.resolver"
    #         - "@request_stack"
    #     tags:
    #         - { name: sylius.context.channel }

# kernel listener pour exception sur contexte channel
    # app.redirect_channel_exception_listener:
    #     class: App\EventListener\RedirectChannelExceptionListener
    #     arguments:
    #         - '@request_stack'
    #     tags:
    #         - { name: kernel.event_listener, event: kernel.exception }

# kernel listener pour contexte channel
    App\EventListener\ChannelContextRequestListener:
        tags:
            - { name: kernel.event_listener, event: kernel.request, method: onRequest, priority: 512 }
