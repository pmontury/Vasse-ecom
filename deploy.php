<?php
namespace Deployer;

require 'recipe/common.php';

//--------------
set('application', 'prod-vasse-e-com');

// Hosts
host('prod')
    ->setLabels([
        'type' => 'front',
        'env' => 'prod',
    ])
    ->setHostname('10.100.58.2')
    ->setRemoteUser('septcinquante')
    ->set('deploy_path', '/home/septcinquante/public_html')
    ->set('branch','master')
    ->setIdentityFile('~/.ssh/id_rsa')
    ;

// Nombre de déploiements à conserver avant de les supprimer.
set('keep_releases', 3);


// Votre repo
set('repository', 'git@gitlab.com:pmontury/Vasse-ecom.git');

set('bin_dir', 'bin');

set('clear_paths', ['var/cache']);

set('shared_files', []); // on ecrase le share_file car on utilise pas .env.local on passe par uen copie de env.prod dans .env
add('shared_files', []); // vous pouvez ajouter des fichiers partagés et surcharger la recette de base
add('shared_dirs', ["/config/jwt","./public/media","./private/invoices"]); // vous pouvez ajouter des dossiers partagés et surcharger la recette de base

task('deploy:writable', function () {
        writeln('Bruno deploy:writable is override with nothing ');
   
});



// vous pouvez surcharger la recette de base en réécrivant la fonction
task('deploy:vendors', function () {
    writeln('install les dépendances');

    if (!commandExist('unzip')) {
        writeln('To speed up composer installation setup "unzip" command with PHP zip extension https://goo.gl/sxzFcD');
    }
    run('cd {{release_path}} && {{bin/composer}} install --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');
});




task('info', function () {
    writeln('type:' . get('labels')['type'] . ' env:' . get('labels')['env']);
});

task('deploy:cache:clear', function () {
    writeln('bruno override cache:clear ');
    run('cd {{release_path}} && bin/console cache:clear --no-warmup --env=prod');
    
});


task('deploy:copy:env:prod', function () {
    writeln('copy les env Prod');
    run('  cp {{release_path}}/.env.prod {{release_path}}/.env'); // copy le fichier envirronement de prod dans .env
    writeln('copy .env.prod in .env');
    
});

// task('deploy:copy:env:dev', function () {
//     writeln('copy les env DEV');
//     run('  cp {{release_path}}/api/.env.dev {{release_path}}/api/.env'); // copy le fichier envirronement de dev(.env) dans .env
//     writeln('copy .env in .env');
    
// });

task('deploy:schema:update', function () {
    writeln('debut database update ');
    run(' cd {{release_path}} && bin/console doctrine:schema:update --complete --force ');
    writeln('database update success');
    
});

task('deploy:fixture:update', function () {
    writeln('LOAD FIXTURES ');
    run(' cd /home/septcinquante/public_html/current && bin/console sylius:fixtures:load vasse_re7 --no-interaction &&  
    bin/console sylius:user:promote ecom-super@vasse.fr ROLE_SUPER_ADMINISTRATION_ACCESS --user-type=admin && \
    bin/console sylius:user:promote ecom-extranet@vasse.fr ROLE_API_EXTRANET_ACCESS --user-type=admin && \
    bin/console sylius:user:demote ecom-extranet@vasse.fr ROLE_ADMINISTRATION_ACCESS --user-type=admin' );
});

task('deploy:fixture:bank', function () {
    writeln('LOAD FIXTURES BANK REF ');
    run(' cd /home/septcinquante/public_html/current && bin/console sylius:fixtures:load vasse_bank_infos --no-interaction' );
});

task('deploy:sylius:assets', function() {
    writeln('set sylius assets  ');
    run(' cd {{release_path}} && bin/console assets:install --no-interaction &&  
    bin/console sylius:theme:assets:install public --no-interaction' );
		
});

task('deploy:set-permissions', function () {
    $httpUser = 'www-data'; 
    run("chown -R septcinquante:$httpUser /home/septcinquante/public_html/current/var");
    run("chmod -R 777 /home/septcinquante/public_html/current/var");
    //run("chown -R $httpUser:$httpUser /home/septcinquante/public_html/shared/public/media");
    //run("chmod -R 777 /home/septcinquante/public_html/shared/public/media");

});

task('deploy:yarn:build', function() {
    writeln('<fg=red;options=bold> ATTENTION ne pas oublier de compiler -> Yarn build:prod en LOCALE avant de deployer </>');
    // run(' cd /home/septcinquante/public_html && yarn install &&  yarn encore production' );
});



task('deploy_prod', [
    'deploy:yarn:build',
    'deploy:prepare',
    'deploy:vendors',
    'deploy:copy:env:prod',
    'deploy:schema:update',
    'deploy:sylius:assets',
    'deploy:cache:clear',
    'deploy:publish',

]);

// première install uniquement 
task('deploy_fixtures_prod', [
    'deploy:fixture:update',
]);

task('deploy_fixture_bank_ref',[
    'deploy:fixture:bank',
]);


task('deploy_dev', [
    'deploy:prepare',
    'deploy:vendors',
    'deploy:copy:env:dev',
    'deploy:schema:update',
    'deploy:cache:clear',
    'deploy:publish',
]);



//after('deploy:shared','deploy:copy_env');
after('deploy:symlink', 'deploy:set-permissions');
after('deploy:failed', 'deploy:unlock');
//--------------


