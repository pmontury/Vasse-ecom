<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240422084651 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sylius_order_item DROP FOREIGN KEY FK_77B587ED7A7D12B9');
        $this->addSql('DROP INDEX IDX_77B587ED7A7D12B9 ON sylius_order_item');
        $this->addSql('ALTER TABLE sylius_order_item DROP channel_stock_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sylius_order_item ADD channel_stock_id INT NOT NULL');
        $this->addSql('ALTER TABLE sylius_order_item ADD CONSTRAINT FK_77B587ED7A7D12B9 FOREIGN KEY (channel_stock_id) REFERENCES sylius_channel_stock (id)');
        $this->addSql('CREATE INDEX IDX_77B587ED7A7D12B9 ON sylius_order_item (channel_stock_id)');
    }
}
