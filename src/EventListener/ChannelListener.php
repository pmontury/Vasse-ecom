<?php

namespace App\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use App\Model\Channel\ChannelInterface;
use App\Model\ExtranetTo\ExtranetEndSale;
use App\Repository\Product\ProductRepositoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use App\Service\ExtranetHttpService;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Sylius\Component\Core\Model\ShippingMethodInterface;

class ChannelListener
{
    private $triggerEndSaleMsg = false;

    public function __construct(
        private EntityManagerInterface $em,
        private ExtranetHttpService $httpService, 
        private RepositoryInterface $paymentMethodRepository,
        private RepositoryInterface $shippingMethodRepository,
        private RepositoryInterface $bankInfosRepository,
        private ProductRepositoryInterface $productRepository) {}

    public function preUpdate(ChannelInterface $channel, PreUpdateEventArgs $preEvent): void
    {
        if ($preEvent->hasChangedField("enabled")) {
            if ($preEvent->getNewValue("enabled") === false) {
                $channel->setDeactivatedAt(new \DateTime("now", new \DateTimeZone('Europe/Paris')));
            } elseif ($preEvent->getNewValue("enabled") === true) {
                $channel->setDeactivatedAt(null);
            }
        }

        if ($preEvent->hasChangedField("saleClosedAt")) {
            $this->triggerEndSaleMsg = true;
        }
    }

    public function postUpdate(ChannelInterface $channel, PostUpdateEventArgs $postEvent): void
    {
        
        if ($this->triggerEndSaleMsg) {
            $this->triggerEndSaleMsg = false;
            $products = $this->productRepository->findAllByChannel($channel);
            // dump(new ExtranetendSale($products, $channel));
            $this->httpService->sendEndSale(new ExtranetendSale($products, $channel));
        }
    }

    public function prePersist(ChannelInterface $channel): void
    {
        $bankInfos = $this->bankInfosRepository->findOneBy(['code'=> 'coordonnees_bancaires_vasse']);
        $channel->setBankInfos($bankInfos);
    }

    public function postPersist(ChannelInterface $channel): void
    {
        $paymentMethods = $this->paymentMethodRepository->findAll();

        /** @var PaymentMethodInterface $paymentMethod */
        foreach ($paymentMethods as $paymentMethod) {
            $paymentMethod->addChannel($channel);
            $this->em->persist($paymentMethod);
        }

        $shippingMethods = $this->shippingMethodRepository->findAll();

        /** @var ShippingMethodInterface $shippingMethod */
        foreach ($shippingMethods as $shippingMethod) {
            $shippingMethod->addChannel($channel);
            if ($channel->getCode() != 'VASSE_MOBILIER') {
                $configuration = $shippingMethod->getConfiguration();
                if ($shippingMethod->getCalculator() == 'flat_rate ') {
                    $configuration[$channel->getCode()] = ['amount' => 0];
                } else {
                    $configuration[$channel->getCode()] = ['amount' => 0];
                }
                $shippingMethod->setConfiguration($configuration);
            }
            $this->em->persist($shippingMethod);
        }

        $this->em->flush();

    }

}