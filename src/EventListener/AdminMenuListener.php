<?php

namespace App\EventListener;

use App\Service\AdminMenu;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;
use Knp\Menu\MenuItem;
use Knp\Menu\FactoryInterface;
use Symfony\Bundle\SecurityBundle\Security;

final class AdminMenuListener
{
    public function __construct(private AdminMenu $adminMenu, private FactoryInterface $factory, private Security $security) 
    {
    }

    public function filterAdminMenuItems(MenuBuilderEvent $event): void
    {
        $configSubMenu = $event->getMenu()->getChild('configuration');

        $menuItem = new MenuItem('coordonnees_bancaires', $this->factory);
        $menuItem->setLabel('Coordonnées bancaires');
        $menuItem->setLabelAttributes(['icon' => 'icon euro sign']);
        $menuItem->setUri('/admin/coordonnees_bancaires/');
        $menuItem->setExtras(['routes' => [['route' => 'app_admin_bank_infos_update'], ['route' => 'app_admin_bank_infos_index']]]);
        $configSubMenu->addChild($menuItem);

        $menuItem = new MenuItem('pick_up_sites', $this->factory);
        $menuItem->setLabel("Sites d'enlèvement");
        $menuItem->setLabelAttributes(['icon' => 'icon warehouse']);
        $menuItem->setUri('/admin/sites_enlevement/');
        $menuItem->setExtras(['routes' => [['route' => 'app_admin_pick_up_sites_update'], ['route' => 'app_admin_pick_up_sites_index']]]);
        $configSubMenu->addChild($menuItem);

        $event->getMenu()->getChild('catalog')->removeChild('inventory');
        $this->adminMenu->filterItems($event->getMenu());
    }

}
