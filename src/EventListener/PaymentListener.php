<?php

namespace App\EventListener;

use SM\Factory\FactoryInterface;
use SM\StateMachine\StateMachineInterface;
use App\Service\ExtranetHttpService;
use App\Model\ExtranetTo\ExtranetNewOrder;
use App\Repository\Order\OrderRepositoryInterface;
use App\Sylius\Component\Core\OrderShippingStates;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use App\Sylius\Component\Core\OrderShippingTransitions;
use Doctrine\ORM\EntityManagerInterface;

final class PaymentListener
{
    public function __construct(private ExtranetHttpService $httpService,
                                private FactoryInterface $stateMachineFactory,
                                private EntityManagerInterface $em, 
                                private OrderRepositoryInterface $orderRepository) {}

    public function processEvent(ResourceControllerEvent $event): void
    {
        $order = $event->getSubject()->getOrder();
        
        if ($order->getShippingState() == OrderShippingStates::STATE_AWAITING_PAYMENT) {
            $order = $this->orderRepository->findOneWithAddressByNumber($order->getNumber());

            $this->httpService->sendNewOrder(new ExtranetNewOrder($order));

            /** @var StateMachineInterface $stateMachine */
            $stateMachine = $this->stateMachineFactory->get($order, OrderShippingTransitions::GRAPH);

            if ($stateMachine->can(OrderShippingTransitions::TRANSITION_SHIPPING_START_PREPARATION)) {
                $stateMachine->apply(OrderShippingTransitions::TRANSITION_SHIPPING_START_PREPARATION);
            }
    
            $this->em->persist($order);
            $this->em->flush();
        }
    }

}
