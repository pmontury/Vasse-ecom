<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\RequestContext;

final class ChannelContextRequestListener
{
    public function __construct(
        private readonly RouterInterface $router,
        private ParameterBagInterface $params
    ) {
    }

    public function onRequest(RequestEvent $event)
    {
        if (!$event->isMainRequest()) return;

        $request = $event->getRequest();

        /** @var RequestContext $context */      
        $context = $this->router->getContext();

        if($request->getPathInfo() == '/') {
            $context->setParameter('_channel', $this->params->get('ecom.default.channel_context'));
        } else {
            if (!$context->hasParameter('_channel')) {
                $context->setParameter('_channel', explode('/', $request->getPathInfo())[1]);
            }
        }
    }

}