<?php

namespace App\EventListener;

use SM\Factory\FactoryInterface;
use SM\StateMachine\StateMachineInterface;
use App\Service\ExtranetHttpService;
use App\Model\ExtranetTo\ExtranetNewOrder;
use App\Model\Product\ProductInterface;
use App\Sylius\Component\Core\OrderShippingStates;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use App\Sylius\Component\Core\OrderShippingTransitions;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

final class ProductListener
{
    public function __construct() {}

    public function resetIsNew(ResourceControllerEvent $event): void
    {
        /** @var ProductInterface */
        $product = $event->getSubject();

        if ($product->isEnabled() && $product->isNew()) {
            $product->setNew(false);
        }
    }

}
