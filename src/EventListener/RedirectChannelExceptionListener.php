<?php

namespace App\EventListener;

use App\Exception\RedirectChannelException;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

class RedirectChannelExceptionListener
{
    public function __construct(private RequestStack $requestStack) {}

    public function __invoke(ExceptionEvent $event)
    {
        if ($event->getThrowable() instanceof RedirectChannelException) {
            $msg = "Cette vente privée n'est pas disponible. Vous avez été redirigé vers Vasse Mobilier.";

            /** @var Session $session */
            $session = $this->requestStack->getSession();
            $session->getFlashBag()->add('warning', $msg);

            $event->setResponse(new RedirectResponse($event->getThrowable()->getMessage()));
        }
    }
}
