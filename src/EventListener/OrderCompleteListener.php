<?php

namespace App\EventListener;

use App\Model\ExtranetTo\ExtranetNewOrder;
use App\Repository\Order\OrderRepository;
use App\Service\ExtranetHttpService;
use App\Sylius\Component\Core\OrderPaymentStates;
use App\Sylius\Component\Core\OrderShippingTransitions;
use Sylius\Component\Core\Model\OrderInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Webmozart\Assert\Assert;
use SM\Factory\FactoryInterface;
use SM\StateMachine\StateMachineInterface;
use Doctrine\ORM\EntityManagerInterface;

final class OrderCompleteListener
{
    public function __construct(private ExtranetHttpService $httpService,
                                private FactoryInterface $stateMachineFactory,
                                private OrderRepository $orderRepository,
                                private EntityManagerInterface $em) {}

    public function processEvent(GenericEvent $event): void
    {
        /** OrderInterface $order */
        $order = $event->getSubject();
        Assert::isInstanceOf($order, OrderInterface::class);

        if ($order->getPaymentState() == OrderPaymentStates::STATE_DEFERRED_PAYMENT) {
            $order = $this->orderRepository->findOneWithAddressByNumber($order->getNumber());
            $extranetNewOrder = new ExtranetNewOrder($order);

            $this->httpService->sendNewOrder($extranetNewOrder);

            /** @var StateMachineInterface $stateMachine */
            $stateMachine = $this->stateMachineFactory->get($order, OrderShippingTransitions::GRAPH);

            if ($stateMachine->can(OrderShippingTransitions::TRANSITION_SHIPPING_START_PREPARATION)) {
                $stateMachine->apply(OrderShippingTransitions::TRANSITION_SHIPPING_START_PREPARATION);
            }
    
            $this->em->persist($order);
            $this->em->flush();
        } 
    }

}
