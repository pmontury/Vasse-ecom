<?php

namespace App\EventListener;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class ProductUpdateMenuListener
{
    public function removeVariantButton(MenuBuilderEvent $event): void
    {
        $event->getMenu()->removeChild('manage_variants');
    }

}
