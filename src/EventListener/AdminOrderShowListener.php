<?php

namespace App\EventListener;

use App\Service\AdminMenu;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class AdminOrderShowListener
{
    public function changeColor(MenuBuilderEvent $event): void
    {
        if ($labelAttributes = $event->getMenu()->getChild('cancel')) {
            $labelAttributes = $event->getMenu()->getChild('cancel')->getLabelAttributes();
            $labelAttributes['color'] = 'red';
            $event->getMenu()->getChild('cancel')->setLabelAttributes($labelAttributes);
        }

        $labelAttributes = $event->getMenu()->getChild('order_history')->getLabelAttributes();
        $labelAttributes['color'] = 'blue';
        $event->getMenu()->getChild('order_history')->setLabelAttributes($labelAttributes);
    }

}
