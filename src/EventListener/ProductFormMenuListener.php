<?php

namespace App\EventListener;

use App\Service\AdminMenu;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class ProductFormMenuListener
{
    private $adminMenu;

    public function __construct(AdminMenu $adminMenu) 
    {
        $this->adminMenu = $adminMenu;
    }

    public function filterAdminFormMenuItems(MenuBuilderEvent $event): void
    {
        $this->adminMenu->filterItems($event->getMenu());
    }

}
