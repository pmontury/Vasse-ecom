<?php

namespace App\EventListener;

use Sylius\Component\Grid\Event\GridDefinitionConverterEvent;
use Sylius\Component\Grid\Definition\Field;

final class AdminProductGridListener
{
    public function removeAction(GridDefinitionConverterEvent $event): void
    {
        $event->getGrid()->removeActionGroup('subitem');
    }
}