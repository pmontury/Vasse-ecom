<?php

namespace App\EventListener;

use App\Service\ExtranetHttpService;
use App\Model\ExtranetTo\ExtranetShippedOrder;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use Sylius\Component\Core\Model\ShipmentInterface;
use Webmozart\Assert\Assert;

final class ShipmentListener
{
    public function __construct(private ExtranetHttpService $httpService) {}

    public function orderIsShipped(ResourceControllerEvent $event): void
    {
        $shipment = $event->getSubject();

        Assert::isInstanceOf($shipment, ShipmentInterface::class);
        
        $this->httpService->sendShippedOrder(new ExtranetShippedOrder($shipment));
    }

}
