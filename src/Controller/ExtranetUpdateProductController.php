<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\Product\ProductVariantInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

use Sylius\Component\Resource\Repository\RepositoryInterface;

class ExtranetUpdateProductController extends AbstractController
{
    const MAX_TRY = 3;
    private $channel;

    public function __construct(private EntityManagerInterface $em, 
                                private RepositoryInterface $productRepository,
                                private RepositoryInterface $productVariantRepository,
                                // private RepositoryInterface $channelStockRepository,
                                // private RepositoryInterface $channelRepository
                                ) {}

    public function __invoke(Request $request, $extranetProdId): Response
    {
        if ($request->getMethod() !== 'PUT') {
            throw new HttpException(Response::HTTP_METHOD_NOT_ALLOWED, 'Méthode ' . $request->getMethod() . ' non autorisée.', null, [], 999);
        }

        $extranetChannelId = $request->getPayload()->get('extranetChannelId');

        $newStock = $request->getPayload()->get('stock');

        if ($newStock === null || !is_numeric($newStock) || $newStock === 0) {
            throw new BadRequestHttpException(json_encode(['message' => "propriété 'stock' absente ou non renseignée", 'prod_id' => $extranetProdId]), null, 999);
        }

        $product = $this->productRepository->findOneBy(['extranetProdId' => $extranetProdId]);

        if (!$product) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Fiche produit id (' . $extranetProdId . ') non trouvée.', null, [], 999);
        }

        // if ($extranetChannelId !== null) {
        //     $this->channel = $this->channelRepository->findOneBy(['extranetChannelId' => $extranetChannelId]);
        //     if (!$this->channel) {
        //         throw new HttpException(Response::HTTP_NOT_FOUND, 'Vente privée id (' . $extranetChannelId . ') non trouvée.', null, [], 999);
        //     }
        // } else {
        //     $this->channel = $this->channelRepository->findOneBy(['main' => true]);
        // }

        // /** @var ProductVariantInterface $productVariant */
        // $productVariant = $product->getVariants()[0];

        // $channelStock = $productVariant->getStockForChannel($this->channel);

        // if ($channelStock) {
        //     $this->updateChannelStock($extranetProdId, $channelStock->getId(), $newStock);
        // } else {
        //     $this->addChannelStock($productVariant, $newStock);
        // }

        $this->updateStock($extranetProdId, $product->getVariants()[0]->getId(), $newStock);

        $response = new Response();
        $response->setStatusCode(Response::HTTP_OK);
        $response->setContent('{prod_id: ' . $extranetProdId . '}');

        return $response;
    }

    private function updateStock($extranetProdId, $productVariantId, $newStock)
    {
        /** @var ProductVariantInterface $productVariant */
        $productVariant = $this->productVariantRepository->find($productVariantId);

        $done = false;
        $nbTry = 0;

        do {
            try {
                $this->em->lock($productVariant, LockMode::OPTIMISTIC, $productVariant->getVersion());
                $productVariant->setOnHand($productVariant->getOnHand() + $newStock);
                $this->em->flush();
                $done = true;
            } catch(OptimisticLockException $e) {
                $nbTry++;
                if ($nbTry == self::MAX_TRY) {
                    $done = true;
                } else {
                    $productVariant = $this->productVariantRepository->find($productVariantId);
                }
            }
        } while (!$done);

        if ($nbTry == self::MAX_TRY) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, 'Mise à jour du stock produit id (' . $extranetProdId . ') impossible.', null, [], 999);
        }
    }

    // private function updateChannelStock($extranetProdId, $channelStockId, $newStock)
    // {
    //     $entity = $this->channelStockRepository->find($channelStockId);

    //     $done = false;
    //     $nbTry = 0;

    //     do {
    //         try {
    //             $this->em->lock($entity, LockMode::OPTIMISTIC, $entity->getVersion());
    //             $entity->setOnHand($entity->getOnHand() + $newStock);
    //             $this->em->flush();
    //             $done = true;
    //         } catch(OptimisticLockException $e) {
    //             $nbTry++;
    //             if ($nbTry == self::MAX_TRY) {
    //                 $done = true;
    //             } else {
    //                 $entity = $this->channelStockRepository->find($channelStockId);
    //             }
    //         }
    //     } while (!$done);

    //     if ($nbTry == self::MAX_TRY) {
    //         throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, 'Mise à jour du stock produit id (' . $extranetProdId . ') impossible.', null, [], 999);
    //     }
    // }

    // private function addChannelStock(ProductVariantInterface $productVariant, $newStock)
    // {
    //     $channelStock = $productVariant->createStock();
    //     $channelStock->setChannelId($this->channel->getId());
    //     $channelStock->setExtranetChannelId($this->channel->getExtranetChannelId());
    //     $channelStock->setOnHand($newStock);
    //     $channelStock->setOnHold(0);
    //     $channelStock->setVersion(1);
    //     $channelStock->setTracked(true);
    //     $channelStock->setChannelCode($this->channel->getCode());
    //     $productVariant->addStock($channelStock);

    //     $this->em->persist($productVariant);
    //     $this->em->flush();
    // }

}
