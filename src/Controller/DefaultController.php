<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends AbstractController
{
    public function __construct(private ParameterBagInterface $params) {}

    public function redirectAction(Request $request, ): response
    {
        return new RedirectResponse($request->getUri() . $this->params->get('ecom.default.channel_context'));
    }

}
