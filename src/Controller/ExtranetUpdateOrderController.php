<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\ExtranetFrom\ExtranetNewOrder;
use App\Service\ExtranetHttpService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

use Sylius\Component\Resource\Repository\RepositoryInterface;
use App\Sylius\Component\Core\OrderShippingTransitions;
use Sylius\Component\Core\OrderShippingStates;

use SM\Factory\FactoryInterface;

class ExtranetUpdateOrderController extends AbstractController
{
    const STATE_READY = 'ready';
    const STATE_NOT_READY = 'not ready';

    public function __construct(private EntityManagerInterface $em, 
                                private RepositoryInterface $orderRepository,
                                private FactoryInterface $stateMachineFactory,
                                private ExtranetHttpService $httpService) {}

    public function __invoke(Request $request, $number): Response
    {
        if ($request->getMethod() !== 'PUT') {
            throw new HttpException(Response::HTTP_METHOD_NOT_ALLOWED, 'Méthode ' . $request->getMethod() . ' non autorisée.', null, [], 999);
        }

        $state = $request->getPayload()->get('state');

        if ($state === null || ($state !== self::STATE_READY && $state !== self::STATE_NOT_READY)) {
            throw new BadRequestHttpException("propriété 'state' absente ou invalide", null, 999);
        }

        $order = $this->orderRepository->findOneBy(['number' => $number]);

        if (!$order) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Commande n° ' . $number . ' non trouvée.', null, [], 999);
        }

        $stateMachine = $this->stateMachineFactory->get($order, OrderShippingTransitions::GRAPH);

        $transition = $state === self::STATE_READY ?OrderShippingTransitions::TRANSITION_READY :OrderShippingTransitions::TRANSITION_UNREADY;

        if ($stateMachine->can($transition)) {
            $stateMachine->apply($transition);
        } else {
            throw new BadRequestHttpException("Transition vers état '" . $state . "' impossible", null, 999);
        }
        
        $this->em->flush();
        
        $response = new Response();
        $response->setStatusCode(204);

        return $response;
    }

}
