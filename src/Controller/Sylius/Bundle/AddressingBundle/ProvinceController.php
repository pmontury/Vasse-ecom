<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Sylius Sp. z o.o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Controller\Sylius\Bundle\AddressingBundle;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sylius\Bundle\AddressingBundle\Controller\ProvinceController as BaseProvinceController;

class ProvinceController extends BaseProvinceController
{
    public function choiceOrTextFieldFormAction(Request $request): Response
    {
        return new JsonResponse([]);
    }

}
