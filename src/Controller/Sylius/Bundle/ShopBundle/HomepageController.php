<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Sylius Sp. z o.o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Controller\Sylius\Bundle\ShopBundle;

use Sylius\Component\Channel\Context\ChannelContextInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Sylius\Component\Channel\Repository\ChannelRepositoryInterface;

final class HomepageController extends AbstractController
{
    public function __construct(
        private Environment $templatingEngine, 
        private ChannelContextInterface $channelContext,
        private ParameterBagInterface $params,
        private ChannelRepositoryInterface $channelRepository
    )
    {
    }

    public function indexAction(Request $request): Response
    {
        $channelName = explode('/', $request->getPathInfo())[1];

        $channel = $this->findChannel($request);

        if (!$channel || !$channel->isEnabled()) {
            $msg = "La vente privée '$channelName' n'est pas disponible. Vous avez été redirigé vers Vasse Mobilier.";
            $this->addFlash('warning', $msg);
            return $this->redirect('/' . $this->params->get('ecom.default.channel_context'));
        }

        return new Response($this->templatingEngine->render('@SyliusShop/Homepage/index.html.twig'));
    }

    private function findChannel(Request $request)
    {
        $host = str_replace('www.', '', $request->getHost());

        $searchHostName = "$host/" . explode('/', $request->getPathInfo())[1];

        return $this->channelRepository->findOneBy(['hostname' => $searchHostName]);
    }

    public function aboutUsAction(Request $request): Response
    {
        $template = $this->getSyliusAttribute($request, 'template', 'app/Shop/AboutUs/about_us.html.twig');
        return new Response($this->templatingEngine->render($template));
    }

    public function showroomAction(Request $request): Response
    {
        $template = $this->getSyliusAttribute($request, 'template', 'app/Shop/Showroom/showroom.html.twig');
        return new Response($this->templatingEngine->render($template));
    }

    public function deliveryAction(Request $request): Response
    {
        $template = $this->getSyliusAttribute($request, 'template', 'app/Shop/Delivery/delivery.html.twig');
        return new Response($this->templatingEngine->render($template));
    }

    public function mentionsLegalesAction(Request $request): Response
    {
        $template = $this->getSyliusAttribute($request, 'template', 'app/Shop/MentionsLegales/mentions_legales.html.twig');
        return new Response($this->templatingEngine->render($template));
    }

    private function getSyliusAttribute(Request $request, string $attribute, $default = null)
    {
        $attributes = $request->attributes->get('_sylius', []);
        return $attributes[$attribute] ?? $default;
    }

}
