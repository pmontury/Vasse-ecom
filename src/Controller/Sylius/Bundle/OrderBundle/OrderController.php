<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Sylius Sp. z o.o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Controller\Sylius\Bundle\OrderBundle;

use App\Model\Payment\PaymentInterface;
use Sylius\Bundle\CoreBundle\Controller\OrderController as BaseOrderController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Webmozart\Assert\Assert;

class OrderController extends BaseOrderController
{
    public function checkOrderPaymentAction(Request $request, $tokenValue): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $orderRepository = $this->getOrderRepository();

        /** @var OrderInterface|null $order */
        $order = $orderRepository->findOneByTokenValue($tokenValue);

        $request->getSession()->set('sylius_order_id', $order->getId());

        /** @var PaymentInterface $payment */
        $payment = $order->getLastPayment(PaymentInterface::STATE_NEW);

        if ($payment->isDeferred()) {
            return $this->redirectHandler->redirectToRoute($configuration, 'sylius_shop_order_thank_you', []);
        }

        $redirect = $configuration->getParameters()->get('redirect');

        return $this->redirectHandler->redirectToRoute($configuration, $redirect['route'], $redirect['parameters']);
    }

}
