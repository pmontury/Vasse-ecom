<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\Product\ProductInterface;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use Sylius\Component\Resource\ResourceActions;
use Sylius\Component\Resource\Exception\UpdateHandlingException;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProductController extends ResourceController
{
    public function bulkTransfertAction(Request $request): Response
    {
        $repository = $this->container->get('sylius.repository.channel');
        $mainChannel = $repository->findOneBy(['main' => true]);

        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, 'bulk_transfert');
        $resources = $this->resourcesCollectionProvider->get($configuration, $this->repository);

        if (
            $configuration->isCsrfProtectionEnabled() &&
            !$this->isCsrfTokenValid('bulk_transfert', (string) $request->request->get('_csrf_token'))
        ) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'Invalid csrf token.');
        }

        $this->eventDispatcher->dispatchMultiple('bulk_transfert', $configuration, $resources);

        foreach ($resources as $resource) 
        {
            foreach ($resource->getChannels() as $channel) {
                $resource->removeChannel($channel);
            }

            $resource->addChannel($mainChannel);

            $event = $this->eventDispatcher->dispatchPreEvent(ResourceActions::UPDATE, $configuration, $resource);

            if ($event->isStopped() && !$configuration->isHtmlRequest()) {
                throw new HttpException($event->getErrorCode(), $event->getMessage());
            }
            if ($event->isStopped()) {
                $this->flashHelper->addFlashFromEvent($configuration, $event);

                $eventResponse = $event->getResponse();
                if (null !== $eventResponse) {
                    return $eventResponse;
                }

                return $this->redirectHandler->redirectToIndex($configuration, $resource);
            }

            try {
                $this->resourceUpdateHandler->handle($resource, $configuration, $this->manager);
            } catch (UpdateHandlingException $exception) {
                if (!$configuration->isHtmlRequest()) {
                    return $this->createRestView($configuration, null, $exception->getApiResponseCode());
                }

                $this->flashHelper->addErrorFlash($configuration, $exception->getFlash());

                return $this->redirectHandler->redirectToReferer($configuration);
            }

            $postEvent = $this->eventDispatcher->dispatchPostEvent(ResourceActions::UPDATE, $configuration, $resource);
        }

        if (!$configuration->isHtmlRequest()) {
            return $this->createRestView($configuration, null, Response::HTTP_NO_CONTENT);
        }

        $this->flashHelper->addSuccessFlash($configuration, 'bulk_transfert');

        if (isset($postEvent)) {
            $postEventResponse = $postEvent->getResponse();
            if (null !== $postEventResponse) {
                return $postEventResponse;
            }
        }

        return $this->redirectHandler->redirectToIndex($configuration);
    }


    public function desactivateAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);

        if ($configuration->isCsrfProtectionEnabled() 
            && !$this->isCsrfTokenValid('desactivate', (string) $request->request->get('_csrf_token'))) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'Invalid csrf token.');
        }

        /** @var ProductInterface $product */
        $product = $this->findOr404($configuration);

        $product->setEnabled(false);

        /** @var ResourceControllerEvent $event */
        $event = $this->eventDispatcher->dispatchPreEvent(ResourceActions::UPDATE, $configuration, $product);

        if ($event->isStopped()) {
            $this->flashHelper->addFlashFromEvent($configuration, $event);

            $eventResponse = $event->getResponse();
            if (null !== $eventResponse) {
                return $eventResponse;
            }

            return $this->redirectHandler->redirectToReferer($configuration);
        }

        try 
        {
            $this->resourceUpdateHandler->handle($product, $configuration, $this->manager);
        } 
        catch (UpdateHandlingException $exception) 
        {
            $this->flashHelper->addErrorFlash($configuration, $exception->getFlash());
            return $this->redirectHandler->redirectToReferer($configuration);
        }

        $this->flashHelper->addSuccessFlash($configuration, 'desactivate');

        $postEvent = $this->eventDispatcher->dispatchPostEvent(ResourceActions::UPDATE, $configuration, $product);

        $postEventResponse = $postEvent->getResponse();
        if (null !== $postEventResponse) {
            return $postEventResponse;
        }

        return $this->redirectHandler->redirectToIndex($configuration);
    }
}
