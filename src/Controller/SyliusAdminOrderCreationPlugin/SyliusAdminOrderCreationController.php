<?php

declare(strict_types=1);

namespace App\Controller\SyliusAdminOrderCreationPlugin;

use App\Sylius\AdminOrderCreationPlugin\Provider\CustomerProvider;
use Sylius\AdminOrderCreationPlugin\Form\Type\NewOrderCustomerCreateType;
use Sylius\AdminOrderCreationPlugin\Form\Type\NewOrderCustomerSelectType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class SyliusAdminOrderCreationController extends AbstractController
{
    public function __construct(private CustomerProvider $customerProvider)
    {
    }

    public function selectCustomer(Request $request): Response
    {
        $selectCustomerForm = $this->createForm(NewOrderCustomerSelectType::class);
        $createCustomerForm = $this->createForm(NewOrderCustomerCreateType::class);

        $selectCustomerForm->handleRequest($request);
        $createCustomerForm->handleRequest($request);

        if ($selectCustomerForm->isSubmitted() || $createCustomerForm->isSubmitted()) {
            if ($selectCustomerForm->isSubmitted() && $selectCustomerForm->isValid()) {
                $customer = $selectCustomerForm->get('customer')->getData();
                $channel = $selectCustomerForm->get('channel')->getData();
                return $this->redirectToRoute('sylius_admin_order_creation_order_create', [
                    'customerId' => $customer->getId(),
                    'channelCode' => $channel->getCode()
                ]);
            }
            if ($createCustomerForm->isSubmitted() && $createCustomerForm->isValid()) {
                $customerEmail = $createCustomerForm->get('customerEmail')->getData();
                $customer = $this->customerProvider->provideNewCustomer($customerEmail);
                $channel = $createCustomerForm->get('channel')->getData();
                return $this->redirectToRoute('sylius_admin_order_creation_order_create', [
                    'customerId' => $customer->getId(),
                    'channelCode' => $channel->getCode()
                ]);
            }
        }

        return $this->render('@SyliusAdminOrderCreationPlugin/Order/selectCustomer.html.twig', [
            'selectCustomerForm' => $selectCustomerForm->createView(),
            'createCustomerForm' => $createCustomerForm->createView(),
        ]);
    }

}
