<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\Channel\ChannelInterface;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use Sylius\Component\Resource\ResourceActions;
use Sylius\Component\Resource\Exception\UpdateHandlingException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ChannelController extends ResourceController
{
    public function clotureAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);

        if ($configuration->isCsrfProtectionEnabled() 
            && !$this->isCsrfTokenValid('cloture', (string) $request->request->get('_csrf_token'))) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'Invalid csrf token.');
        }

        /** @var ChannelInterface $channel */
        $channel = $this->findOr404($configuration);

        $orderRepository = $this->container->get('sylius.repository.order');
        $countOrders = $orderRepository->countUnfulfilledForChannel($channel);

        if ($countOrders > 0) {
            $this->flashHelper->addErrorFlash($configuration, 'cloture_orders_unfulfilled');
            return $this->redirectHandler->redirectToReferer($configuration);
        }

        $channel->setSaleClosedAt(new \DateTimeImmutable("now", new \DateTimeZone('Europe/Paris')));

        /** @var ResourceControllerEvent $event */
        $event = $this->eventDispatcher->dispatchPreEvent(ResourceActions::UPDATE, $configuration, $channel);

        if ($event->isStopped()) {
            $this->flashHelper->addFlashFromEvent($configuration, $event);

            $eventResponse = $event->getResponse();
            if (null !== $eventResponse) {
                return $eventResponse;
            }

            return $this->redirectHandler->redirectToReferer($configuration);
        }

        try 
        {
            $this->resourceUpdateHandler->handle($channel, $configuration, $this->manager);
        } 
        catch (UpdateHandlingException $exception) 
        {
            $this->flashHelper->addErrorFlash($configuration, $exception->getFlash());

            return $this->redirectHandler->redirectToReferer($configuration);
        }

        $this->flashHelper->addSuccessFlash($configuration, 'cloture');

        $postEvent = $this->eventDispatcher->dispatchPostEvent(ResourceActions::UPDATE, $configuration, $channel);

        $postEventResponse = $postEvent->getResponse();
        if (null !== $postEventResponse) {
            return $postEventResponse;
        }

        return $this->redirectHandler->redirectToIndex($configuration);
    }
}