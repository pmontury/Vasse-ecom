<?php

namespace App\Exception;

class RedirectChannelException extends \Exception
{
    public function __construct(
        $message = '',
        $code = 0,
        \Exception $previousException = null
    ) {
        parent::__construct($message, $code, $previousException);
    }
}