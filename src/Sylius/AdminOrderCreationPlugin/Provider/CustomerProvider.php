<?php

declare(strict_types=1);

namespace App\Sylius\AdminOrderCreationPlugin\Provider;

use Sylius\AdminOrderCreationPlugin\Provider\CustomerProviderInterface;
use Sylius\Component\Core\Model\CustomerInterface;
use Sylius\Component\Core\Repository\CustomerRepositoryInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Webmozart\Assert\Assert;
use Symfony\Component\DependencyInjection\Attribute\AsDecorator;

#[AsDecorator('Sylius\AdminOrderCreationPlugin\Provider\CustomerProvider')]

final class CustomerProvider implements CustomerProviderInterface
{
    /** @var CustomerRepositoryInterface */
    private $customerRepository;

    /** @var FactoryInterface */
    private $customerFactory;

    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        FactoryInterface $customerFactory,
    ) {
        $this->customerRepository = $customerRepository;
        $this->customerFactory = $customerFactory;
    }

    /** {@inheritdoc}  */
    public function provideExistingCustomer(string $id): CustomerInterface
    {
        /** @var CustomerInterface $customer */
        $customer = $this->customerRepository->find($id);
        Assert::notNull($customer);

        return $customer;
    }

    /** {@inheritdoc}  */
    public function provideNewCustomer(string $email): CustomerInterface
    {
        /** @var CustomerInterface $customer */
        $customer = $this->customerRepository->findOneBy(['email' => $email]);

        if ($customer) return $customer;

        $customer = $this->customerFactory->createNew();

        $customer->setEmail($email);

        $this->customerRepository->add($customer);

        return $customer;
    }
}
