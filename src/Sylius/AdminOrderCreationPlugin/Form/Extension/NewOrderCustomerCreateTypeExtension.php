<?php

declare(strict_types=1);

namespace App\Sylius\AdminOrderCreationPlugin\Form\Extension;

use Sylius\AdminOrderCreationPlugin\Form\Type\NewOrderCustomerCreateType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

final class NewOrderCustomerCreateTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options): void 
        {
            $form = $event->getForm();
            $form
                ->add('customerEmail', EmailType::class, [
                    'label' => false,
                    'constraints' => [
                        new Email([
                            'message' => 'Vous devez saisir une adresse mail valide',
                            // 'groups' => ['sylius']
                        ])
                    ]
                ])
            ;
        });
    }

    public static function getExtendedTypes(): iterable
    {
        return [NewOrderCustomerCreateType::class];
    }

    public function getBlockPrefix()
    {
        return 'sylius_admin_order_creation_new_order_customer_create';
    }
}
