<?php

declare(strict_types=1);

namespace App\Sylius\AdminOrderCreationPlugin\Form\Extension;

use App\Model\Product\ProductVariantInterface;
use Sylius\AdminOrderCreationPlugin\Form\Type\AdjustmentType;
use Sylius\AdminOrderCreationPlugin\Form\Type\OrderItemType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceAutocompleteChoiceType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

final class OrderItemTypeExtension extends AbstractTypeExtension
{
    /** @var DataMapperInterface */
    private $dataMapper;

    // public function __construct(
    //     string $dataClass,
    //     DataMapperInterface $dataMapper,
    //     array $validationGroups = [],
    // ) {
    //     parent::__construct($dataClass, $validationGroups);

    //     $this->dataMapper = $dataMapper;
    // }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options): void {
                $event
                    ->getForm()
                    ->add('quantity', IntegerType::class, [
                        // 'attr' => ['min' => 1],
                        'label' => 'sylius.ui.quantity',
                        'empty_data' => null
                    ])
                    ->add('variant', ResourceAutocompleteChoiceType::class, [
                        'label' => 'sylius.ui.product',
                        'choice_name' => 'descriptor',
                        'choice_value' => 'code',
                        'resource' => 'sylius.product_variant',
                        'empty_data' => null,
                        'required' => true,
                        'constraints' => [
                            new NotBlank(),
                            new NotNull()
                        ]
                    ])
                ;
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event): void {
                /** @var array $data */
                $data = $event->getData();

                if (empty($data['variant']) && empty($data['quantity'])) {
                    $data = [];
                } else 
                if (!empty($data['variant']) && empty($data['quantity'])) {
                    $data['quantity'] = '1';
                }

                $event->setData($data);
            })
            // ->setDataMapper($this->dataMapper)
        ;
    }

    public static function getExtendedTypes(): iterable
    {
        return [OrderItemType::class];
    }

    public function getParent(): string
    {
        return OrderItemType::class;
    }
}
