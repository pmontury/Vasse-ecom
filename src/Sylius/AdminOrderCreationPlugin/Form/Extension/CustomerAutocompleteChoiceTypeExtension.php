<?php

declare(strict_types=1);

namespace App\Sylius\AdminOrderCreationPlugin\Form\Extension;

use Sylius\AdminOrderCreationPlugin\Form\Type\CustomerAutocompleteChoiceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceAutocompleteChoiceType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class CustomerAutocompleteChoiceTypeExtension extends AbstractTypeExtension
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'resource' => 'sylius.customer',
            'choice_name' => 'email',
            'choice_value' => 'email',
            'label' => false,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['remote_criteria_type'] = 'contains';
        $view->vars['remote_criteria_name'] = 'email';
    }

    public static function getExtendedTypes(): iterable
    {
        return [CustomerAutocompleteChoiceType::class];
    }

    /**
     * @inheritdoc
     */
    public function getBlockPrefix(): string
    {
        return 'sylius_customer_autocomplete_choice';
    }

    /**
     * @inheritdoc
     */
    public function getParent(): string
    {
        return ResourceAutocompleteChoiceType::class;
    }
}
