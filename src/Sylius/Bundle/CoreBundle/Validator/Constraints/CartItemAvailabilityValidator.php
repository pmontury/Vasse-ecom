<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Sylius Sp. z o.o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Sylius\Bundle\CoreBundle\Validator\Constraints;

use App\Model\Product\ProductVariantInterface;
use Sylius\Bundle\OrderBundle\Controller\AddToCartCommandInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\OrderItemInterface;
use Sylius\Component\Inventory\Checker\AvailabilityCheckerInterface;
use Symfony\Component\DependencyInjection\Attribute\AsDecorator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Webmozart\Assert\Assert;

//#[AsDecorator('sylius.validator.cart_item_availability')]

final class CartItemAvailabilityValidator extends ConstraintValidator
{
    public function __construct(private AvailabilityCheckerInterface $availabilityChecker)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        /** @var AddToCartCommandInterface $value */
        Assert::isInstanceOf($value, AddToCartCommandInterface::class);

        // /** @var CartItemAvailability $constraint */
        // Assert::isInstanceOf($constraint, CartItemAvailability::class);

        /** @var OrderItemInterface $cartItem */
        $cartItem = $value->getCartItem();

        // /** @var OrderInterface */
        // $cart = $value->getCart();

        // /** @var ProductVariantInterface $productVariant */
        // $productVariant = $cartItem->getVariant();

        // $channelStock = $productVariant->getStockForChannel($cart->getChannel());

        $isStockSufficient = $this->availabilityChecker->isStockSufficient(
            // $channelStock,
            $cartItem->getVariant(),
            $cartItem->getQuantity() + $this->getExistingCartItemQuantityFromCart($value->getCart(), $cartItem),
        );

        if (!$isStockSufficient) {
            $this->context->addViolation(
                $constraint->message,
                [
                    '%itemName%' => $cartItem->getVariant()->getInventoryName(),
                    // '%qtyDispo%' => $channelStock->getOnHand() - $channelStock->getOnHold()
                    '%qtyDispo%' => $cartItem->getVariant()->getOnHand() - $cartItem->getVariant()->getOnHold()
                ],
            );
        }
    }

    private function getExistingCartItemQuantityFromCart(OrderInterface $cart, OrderItemInterface $cartItem): int
    {
        foreach ($cart->getItems() as $existingCartItem) {
            if ($existingCartItem->equals($cartItem)) {
                return $existingCartItem->getQuantity();
            }
        }

        return 0;
    }
}
