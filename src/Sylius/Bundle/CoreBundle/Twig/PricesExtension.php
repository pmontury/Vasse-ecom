<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Sylius Sp. z o.o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Sylius\Bundle\CoreBundle\Twig;

use App\Sylius\Bundle\CoreBundle\Templating\Helper\PriceHelper;
use Symfony\Component\DependencyInjection\Attribute\AsDecorator;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

#[AsDecorator('sylius.twig.extension.price')]

final class PricesExtension extends AbstractExtension
{
    public function __construct(private PriceHelper $helper)
    {
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('sylius_calculate_price', [$this->helper, 'getPrice']),
            new TwigFilter('sylius_calculate_price_ttc', [$this->helper, 'getPriceTTC']),
            new TwigFilter('sylius_calculate_original_price', [$this->helper, 'getOriginalPrice']),
            new TwigFilter('sylius_has_discount', [$this->helper, 'hasDiscount']),
        ];
    }
}
