<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Sylius Sp. z o.o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Sylius\Component\Channel\Context\RequestBased;

use Sylius\Component\Channel\Context\RequestBased\RequestResolverInterface;
use Sylius\Component\Channel\Model\ChannelInterface;
use Sylius\Component\Channel\Repository\ChannelRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;

final class HostnameBasedRequestResolver implements RequestResolverInterface
{
    public function __construct(private ChannelRepositoryInterface $channelRepository) {}

    public function findChannel(Request $request): ?ChannelInterface
    {
        $host = str_replace('www.', '', $request->getHost());

        $searchHostName = "$host/" . explode('/', $request->getPathInfo())[1];

        return $this->channelRepository->findOneBy(['hostname' => $searchHostName]);
    }

}
