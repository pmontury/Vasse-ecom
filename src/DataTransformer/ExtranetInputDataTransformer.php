<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;

use App\Entity\Product\Product;
use App\Entity\Channel\Channel;

use App\Model\ExtranetFrom\ExtranetProductModel;
use App\Model\ExtranetFrom\ExtranetChannelModel;

use App\DataTransformer\Factory\ExtranetAddProductFactory;
use App\DataTransformer\Factory\ExtranetAddChannelFactory;

final class ExtranetInputDataTransformer implements DataTransformerInterface
{
    private $extranetFactory;

    public function __construct(
        private ExtranetAddProductFactory $extranetAddProductFactory,
        private ExtranetAddChannelFactory $extranetAddChannelFactory,
    ) {}

    /**
     * {@inheritdoc}
     */
    public function transform($data, string $to, array $context = [])
    {
        return $this->extranetFactory->transform($data, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        $this->extranetFactory = null;

        if ($data instanceof Product) {
            return false;
        }

        return $this->isExtranetProductRequest($to, $context)
               || $this->isExtranetChannelRequest($to, $context);
    }

    private function isExtranetProductRequest(string $to, array $context = []): bool
    {
        if (Product::class === $to && $context['input']['class'] === ExtranetProductModel::class) {
            if (isset($context['collection_operation_name']) && $context['collection_operation_name'] === 'extranet_post') {
                $this->extranetFactory = $this->extranetAddProductFactory;
                return true;
            }
        }

        return false;
    }

    private function isExtranetChannelRequest(string $to, array $context = []): bool
    {
        if (Channel::class === $to && $context['input']['class'] === ExtranetChannelModel::class) {
            $this->extranetFactory = $this->extranetAddChannelFactory;
            return true;
        }

        return false;
    }

}