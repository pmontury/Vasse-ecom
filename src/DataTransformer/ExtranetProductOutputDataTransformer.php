<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Entity\Product\Product;
use App\Model\ExtranetFrom\ExtranetProductOutputModel;

final class ExtranetProductOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($data, string $to, array $context = [])
    {
        $output = new ExtranetProductOutputModel();
        $output->setProdId($data->getExtranetProdId());
        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return ExtranetProductOutputModel::class === $to && $data instanceof Product;
    }
}