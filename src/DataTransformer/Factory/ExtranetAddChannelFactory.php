<?php

namespace App\DataTransformer\Factory;

use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use App\Model\ExtranetFrom\ExtranetChannelInterface;
use App\Model\Channel\ChannelInterface;
use DateTimeImmutable;
use Sylius\Component\Channel\Factory\ChannelFactoryInterface;
use Sylius\Component\Core\Model\ShopBillingDataInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;
use Sylius\Component\Product\Generator\SlugGeneratorInterface;

final class ExtranetAddChannelFactory
{
    public function __construct(
        private EntityManagerInterface $em,
        private ChannelFactoryInterface $channelFactory,
        private RepositoryInterface $channelRepository,
        private RepositoryInterface $localeRepository,
        private RepositoryInterface $currencyRepository,
        private RepositoryInterface $zoneRepository,
        private RepositoryInterface $countryRepository,
        private RepositoryInterface $shopBillingDataRepository,
        private TaxonRepositoryInterface $taxonRepository,
        private ParameterBagInterface $params,
        private FactoryInterface $shopBillingDataFactory,
        private SlugGeneratorInterface $slugGenerator,
    ) {
    }

    public function transform(ExtranetChannelInterface $data): ChannelInterface
    {
        $this->controlData($data);
        return $this->setEntity($data);
    }

    public function controlData(ExtranetChannelInterface $data): void
    {
        $errorMsg = '';

        if (!property_exists($data, 'extranetChannelId') || !is_numeric($data->getExtranetChannelId())) {
            $errorMsg = "propriété 'extranetChannelId' absente ou non renseignée";
        } else if ($this->channelRepository->findOneBy(['extranetChannelId' => $data->getExtranetChannelId()])) {
            $errorMsg = "Une vente avec cet id (" . $data->getExtranetChannelId() . ") existe déjà";
        }

        if (!property_exists($data, 'client') || !$data->getClient()) {
            $errorMsg = "propriété 'client' absente ou non renseignée";
        }
        if (!property_exists($data, 'libelle') || !$data->getLibelle()) {
            $errorMsg = "propriété 'libelle' absente ou non renseignée";
        }
        if (!property_exists($data, 'dateDebut') || !is_numeric($data->getDateDebut()) || $data->getDateDebut() === 0) {
            $errorMsg = "propriété 'dateDebut' absente ou non renseignée";
        }
        if (!property_exists($data, 'dateFin') || !is_numeric($data->getDateFin()) || $data->getDateFin() === 0) {
            $errorMsg = "propriété 'dateFin' absente ou non renseignée";
        }

        if ($errorMsg) {
            throw new BadRequestHttpException($errorMsg, null, 999);
        }
    }

    public function setEntity(ExtranetChannelInterface $data): ChannelInterface
    {
        $date = new DateTimeImmutable;
        $nameSlug = $this->slugGenerator->generate($data->getClient());

        /** @var ChannelInterface $channel */
        $channel = $this->channelFactory->createNamed($data->getLibelle());

        $channel->setCode(sprintf('%s_%u', $nameSlug, $data->getExtranetChannelId()));
        $channel->setDescription($data->getClient());
        $channel->setHostname(sprintf('%s/%s_%u', $this->params->get('sylius.channel.hostname'), $nameSlug, $data->getExtranetChannelId()));
        $channel->setEnabled(false);
        $channel->setTaxCalculationStrategy('order_items_based');
        $channel->setThemeName($this->params->get('sylius_theme_name'));
        $channel->setContactEmail($this->params->get('sylius.channel.contact_email'));
        $channel->setContactPhoneNumber($this->params->get('sylius.channel.contact_phone_number'));
        $channel->setSkippingShippingStepAllowed(false);
        $channel->setSkippingPaymentStepAllowed(false);
        $channel->setAccountVerificationRequired(true);
        $channel->setShippingAddressInCheckoutRequired(true);
        $channel->setMain(false);
        $channel->setClient($data->getClient());
        $channel->setExtranetChannelId($data->getExtranetChannelId());
        $channel->setDateDebut($date->setTimestamp($data->getDateDebut()));
        $channel->setDateFin($date->setTimestamp($data->getDateFin()));
        $channel->setShopBillingData($this->setShopBillingData());
        
        $this->setDefaultTaxZone($channel);
        $this->setDefaultLocale($channel);
        $this->setBaseCurrency($channel);
        $this->setCountry($channel);
        $this->setMenuTaxon($channel);

        return $channel;
    }

    private function setDefaultTaxZone(ChannelInterface $channel): void 
    {
        $taxeZone = $this->zoneRepository->findOneBy(['scope' => 'all']);
        $channel->setDefaultTaxZone($taxeZone);
    }

    private function setDefaultLocale(ChannelInterface $channel) : void 
    {
        $defaultLocale = $this->localeRepository->findOneBy(['code' => 'fr_FR']);
        $channel->setDefaultLocale($defaultLocale);
        $channel->addLocale($defaultLocale);
    }

    private function setBaseCurrency(ChannelInterface $channel) : void 
    {
        $baseCurrency = $this->currencyRepository->findOneBy(['code' => 'EUR']);
        $channel->setBaseCurrency($baseCurrency);
        $channel->addCurrency($baseCurrency);
    }

    private function setCountry(ChannelInterface $channel) : void 
    {
        $country = $this->countryRepository->findOneBy(['code' => 'FR']);
        $channel->addCountry($country);
    }

    private function setMenuTaxon(ChannelInterface $channel) : void 
    {
        $menuTaxon = $this->taxonRepository->findOneBy(['code' => 'CATEGORIES']);
        $channel->setMenuTaxon($menuTaxon);
    }

    private function setShopBillingData(): ShopBillingDataInterface
    {
        /** @var ShopBillingDataInterface $shopBillingData */
        $shopBillingData = $this->shopBillingDataRepository->findOneBy(['company' => 'Vasse Mobilier']);

        /** @var ShopBillingDataInterface $newShopBillingData */
        $newShopBillingData = $this->shopBillingDataFactory->createNew();

        $newShopBillingData->setCompany($shopBillingData->getCompany());
        $newShopBillingData->setTaxId($shopBillingData->getTaxId());
        $newShopBillingData->setCountryCode($shopBillingData->getCountryCode());
        $newShopBillingData->setStreet($shopBillingData->getStreet());
        $newShopBillingData->setCity($shopBillingData->getCity());
        $newShopBillingData->setPostcode($shopBillingData->getPostcode());

        $this->em->persist($newShopBillingData);
        $this->em->flush();

        return $newShopBillingData;
    }

}