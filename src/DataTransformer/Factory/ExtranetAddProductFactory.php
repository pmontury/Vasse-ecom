<?php

namespace App\DataTransformer\Factory;

use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;

use App\Model\Product\ProductInterface;
use App\Model\Product\ProductVariantInterface;
use App\Model\ExtranetFrom\ExtranetProductInterface;
use App\ChannelInterface;

use Sylius\Component\Core\Model\TaxonInterface;
use Sylius\Component\Locale\Model\LocaleInterface;
use Sylius\Component\Core\Model\ChannelPricingInterface;
use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Core\Model\ProductTaxonInterface;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Product\Model\ProductAttributeValueInterface;

use Sylius\Component\Product\Generator\ProductVariantGeneratorInterface;
use Sylius\Component\Taxonomy\Generator\TaxonSlugGeneratorInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Product\Generator\SlugGeneratorInterface;
use Sylius\Component\Core\Uploader\ImageUploaderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final class ExtranetAddProductFactory
{
    /** @var ChannelInterface $channel */
    private $channel;

    public function __construct(
        private EntityManagerInterface $em,

        private FactoryInterface $productFactory,
        private FactoryInterface $taxonFactory,
        private FactoryInterface $productVariantFactory,
        private FactoryInterface $channelPricingFactory,
        private FactoryInterface $productAttributeValueFactory,
        private FactoryInterface $productImageFactory,
        private FactoryInterface $productTaxonFactory,

        private TaxonSlugGeneratorInterface $taxonSlugGenerator,
        private SlugGeneratorInterface $slugGenerator,
        private ProductVariantGeneratorInterface $variantGenerator,

        private RepositoryInterface $productRepository,
        private RepositoryInterface $taxonRepository,
        private RepositoryInterface $localeRepository,
        private RepositoryInterface $channelRepository,
        private RepositoryInterface $taxCategoryRepository,
        private RepositoryInterface $productAttributeRepository,

        private ParameterBagInterface $params,
        private ImageUploaderInterface $imageUploader,
        private Filesystem $filesystem,
        private ?FileLocatorInterface $fileLocator = null,
    ) {
    }

    public function transform(ExtranetProductInterface $data, array $context = []): ProductInterface
    {
        $this->controlData($data);
        return $this->setEntity($data, $context);
    }

    private function controlData(ExtranetProductInterface $data)
    {
        $errorMsg = '';

        if (!property_exists($data, 'name') || !$data->getName()) {
            $errorMsg = "propriété 'name' absente ou non renseignée";
        }
        // if (!property_exists($data, 'internalReference') || !$data->getInternalReference()) {
        //     $errorMsg = "propriété 'InternalReference' absente ou non renseignée";
        // }
        if (!property_exists($data, 'category') || !$data->getCategory()) {
            $errorMsg = "propriété 'category' absente ou non renseignée";
        }
        if (!property_exists($data, 'stock') || !is_numeric($data->getStock()) || $data->getStock() === 0) {
            $errorMsg = "propriété 'stock' absente ou non renseignée";
        }
        // if (!property_exists($data, 'brand') || !$data->getBrand()) {
        //     $errorMsg = "propriété 'brand' absente ou non renseignée";
        // }
        // if (!property_exists($data, 'color') || !$data->getColor()) {
        //     $errorMsg = "propriété 'color' absente ou non renseignée";
        // }
        // if (!property_exists($data, 'images') || !$data->getImages() || count($data->getImages()) == 0) {
        //     $errorMsg = "propriété 'images' absente ou non renseignée";
        // }
        if (!property_exists($data, 'extranetProdId') || $data->getExtranetProdId() === null) {
            $errorMsg = "propriété 'extranetProdId' absente ou non renseignée";
        } else if ($this->productRepository->findOneBy(['extranetProdId' => $data->getExtranetProdId()])) {
            $errorMsg = "Un produit avec cet id (" . $data->getExtranetProdId() . ") existe déjà";
        }

        $extranetChannelId = $data->getExtranetChannelId();
        if ($extranetChannelId !== null) {
            $this->channel = $this->channelRepository->findOneBy(['extranetChannelId' => $extranetChannelId]);
            if (!$this->channel) {
                $errorMsg = "Vente id (" . $data->getExtranetChannelId() . ") n'existe pas";
            }
        } else {
            $this->channel = $this->channelRepository->findOneBy(['main' => true]);
        }

        if ($errorMsg) {
            throw new BadRequestHttpException(json_encode(['message' => $errorMsg, 'prod_id' => $data->getExtranetProdId() ?? 'non renseigné']), null, 999);
        }
    }

    private function setEntity(ExtranetProductInterface $data, array $context = []): ProductInterface
    {
        /** @var ProductInterface $product */
        $product = $this->productFactory->createNew();

        $product->setCode($data->getInternalReference());
        $product->setEnabled(false);
        $product->setVariantSelectionMethod(ProductInterface::VARIANT_SELECTION_MATCH);
        $product->setAverageRating(0.0);
        $product->setInternalReference($data->getInternalReference() ?$data->getInternalReference() :null);
        $product->setNew(true);
        $product->setExtranetProdId($data->getExtranetProdId());

        $this->setMainTaxon($product, $data);
        $this->setProductTranslations($product, $data);
        $this->setChannels($product);
        $this->setAttributes($product, $data);
        $this->setVariants($product, $data);
        $this->setImage($product, $data);

        return $product;
    }

    private function setMainTaxon(ProductInterface $product, ExtranetProductInterface $data): void
    {
        $codeSlug = $this->slugGenerator->generate($data->getCategory());

        /** @var TaxonInterface $mainTaxon */
        $mainTaxon = $this->taxonRepository->findOneBy(['code' => $codeSlug]);

        if (!$mainTaxon) {
            $mainTaxon = $this->createTaxon($data);
        }
        
        /** @var ProductTaxonInterface $productTaxon */
        $productTaxon = $this->productTaxonFactory->createNew();
        $productTaxon->setProduct($product);
        $productTaxon->setTaxon($mainTaxon);

        $product->addProductTaxon($productTaxon);
        $product->setMainTaxon($mainTaxon);
    }

    protected function createTaxon(ExtranetProductInterface $data): ?TaxonInterface
    {
        /** @var TaxonInterface $newTaxon */
        $newTaxon = $this->taxonFactory->createNew();
        $parentTaxon = $this->taxonRepository->FindOneBy(['parent' => null]);

        $newTaxon->setCode($this->slugGenerator->generate($data->getCategory()));
        $newTaxon->setParent($parentTaxon);

        // add translation for each defined locales
        foreach ($this->getLocales() as $localeCode) {
            $this->createTaxonTranslations($newTaxon, $localeCode, $data);
        }

        $this->em->persist($newTaxon);
        $this->em->flush();

        return $newTaxon;
    }

    protected function createTaxonTranslations(TaxonInterface $newTaxon, string $localeCode, ExtranetProductInterface $data): void
    {
        $newTaxon->setCurrentLocale($localeCode);
        $newTaxon->setFallbackLocale($localeCode);
        $newTaxon->setName($data->getCategory());
        $newTaxon->setDescription($data->getCategory());
        $newTaxon->setSlug($this->taxonSlugGenerator->generate($newTaxon, $localeCode));
    }

    private function setProductTranslations(ProductInterface $product, ExtranetProductInterface $data): void
    {
        foreach ($this->getLocales() as $localeCode) {
            $product->setCurrentLocale($localeCode);
            $product->setFallbackLocale($localeCode);

            $product->setName($data->getName());
            $product->setSlug($this->slugGenerator->generate(sprintf('%s_%d', $data->getName(), $data->getExtranetProdId())));
            $product->setDescription($data->getName());
            $product->setShortDescription($data->getName());
        }
    }

    private function setChannels($product): void
    {
        $product->addChannel($this->channel);
    }

    private function setAttributes(ProductInterface $product, ExtranetProductInterface $data): void
    {
        if ($data->getBrand()) $this->setProductAttribute($product, 'marque', $data->getBrand());
        if ($data->getColor()) $this->setProductAttribute($product, 'couleur', $data->getColor());
    }

    private function setProductAttribute(ProductInterface $product, $code, $value): void
    {
        /** @var ProductAttributeInterface|null $productAttribute */
        $productAttribute = $this->productAttributeRepository->findOneBy(['code' => $code]);

        if ($productAttribute) {
            foreach ($this->getLocales() as $localeCode) {
                $productAttributeValue = $this->setProductAttributeValue($productAttribute, $localeCode, $value);
                $product->addAttribute($productAttributeValue);
            }
        }
    }

    private function setProductAttributeValue(ProductAttributeInterface $productAttribute, ?string $localeCode, $value): ProductAttributeValueInterface
    {
        /** @var ProductAttributeValueInterface $productAttributeValue */
        $productAttributeValue = $this->productAttributeValueFactory->createNew();

        $productAttributeValue->setAttribute($productAttribute);
        $productAttributeValue->setValue($value);
        $productAttributeValue->setLocaleCode($localeCode);
        
        return $productAttributeValue;
    }

    private function setVariants(ProductInterface $product, ExtranetProductInterface $data): void
    {
        /** @var ProductVariantInterface $productVariant */
        $productVariant = $this->productVariantFactory->createNew();

        $product->addVariant($productVariant);

        $i = 0;
        /** @var ProductVariantInterface $productVariant */
        foreach ($product->getVariants() as $productVariant) {
            $productVariant->setName($data->getName());
            $productVariant->setCode(sprintf('variant-%d', $data->getExtranetProdId()));
            $productVariant->setOnHand($data->getStock());
            $productVariant->setOnHold(0);
            $productVariant->setTaxCategory($this->taxCategoryRepository->findOneBy(['code' => 'tva']));
            $productVariant->setTracked(true);
            $productVariant->setHeight($data->getHeight() > 0 ?$data->getHeight() :null);
            $productVariant->setWidth($data->getWidth() > 0 ?$data->getWidth() :null);
            $productVariant->setDepth($data->getDepth() > 0 ?$data->getDepth() :null);
            $productVariant->setVolume($data->getVolume() > 0 ?$data->getVolume() :null);

            /** @var ChannelInterface $channel */
            foreach ($this->channelRepository->findAll() as $channel) {
                $this->createChannelPricings($productVariant, $channel->getCode(), 0);
            }

            // $this->createChannelStock($productVariant, $data->getStock());
            ++$i;
        }
    }

    private function createChannelPricings(ProductVariantInterface $productVariant, string $channelCode, int $price): void
    {
        /** @var ChannelPricingInterface $channelPricing */
        $channelPricing = $this->channelPricingFactory->createNew();

        $channelPricing->setChannelCode($channelCode);
        $channelPricing->setPrice($price);
        $productVariant->addChannelPricing($channelPricing);
    }

    // private function createChannelStock(ProductVariantInterface $productVariant, $newStock)
    // {
    //     $channelStock = $productVariant->createStock();
    //     $channelStock->setChannelId($this->channel->getId());
    //     $channelStock->setExtranetChannelId($this->channel->getExtranetChannelId());
    //     $channelStock->setOnHand($newStock);
    //     $channelStock->setOnHold(0);
    //     $channelStock->setVersion(1);
    //     $channelStock->setTracked(true);
    //     $channelStock->setChannelCode($this->channel->getCode());
    //     $productVariant->addStock($channelStock);
    // }

    private function setImage(ProductInterface $product, ExtranetProductInterface $data): void
    {
        if (!$data->getImages()) return;
        
        $imagesPath = $data->getImages();
        $imageType = null;

        foreach ($imagesPath as $imagePath) 
        {
            if (false === ($fileContents = $this->httpGetFileContent($imagePath))) continue;

            if (!$this->filesystem->exists($this->params->get('app.temp_image.dir'))) {
                $this->filesystem->mkdir($this->params->get('app.temp_image.dir'));
            }

            $tempFile_path = tempnam($this->params->get('app.temp_image.dir'), '');
            
            $product->tempFile_path[] = $tempFile_path;

            // if (!$fileContents || file_put_contents($tempFile_path, $fileContents) === false) {
            //     throw new BadRequestHttpException(json_encode(['message' => 'Problème téléchargement fichier ' + $imagePath, 'prod_id' => $data->getExtranetProdId()]), null, 999);
            // }

            if (!$fileContents || file_put_contents($tempFile_path, $fileContents) === false) continue;

            $tempImagePath = $this->fileLocator === null ? $tempFile_path : $this->fileLocator->locate($tempFile_path);

            $uploadedImage = new UploadedFile($tempImagePath, basename($tempImagePath), test: true);
    
            /** @var ImageInterface $productImage */
            $productImage = $this->productImageFactory->createNew();
            $productImage->setFile($uploadedImage);
            $productImage->setType($imageType);

            $this->imageUploader->upload($productImage);

            $product->addImage($productImage);
        }
    }

    private function httpGetFileContent($url)
    {
        if(!$url || !is_string($url) || ! preg_match('/^http(s)?:\/\/[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(\/.*)?$/i', $url)){
            return false;
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        $fileContents = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($httpcode !== 200) return false;

        return $fileContents;
    }


    private function getLocales(): iterable
    {
        /** @var LocaleInterface[] $locales */
        $locales = $this->localeRepository->findAll();

        foreach ($locales as $locale) {
            yield $locale->getCode();
        }
    }    

}