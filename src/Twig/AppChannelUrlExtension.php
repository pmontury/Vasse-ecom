<?php

declare(strict_types=1);

namespace App\Twig;

use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Channel\Model\ChannelInterface;
use Symfony\Component\HttpFoundation\UrlHelper;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class AppChannelUrlExtension extends AbstractExtension
{
    private ChannelContextInterface $channelContext;

    private UrlHelper $urlHelper;

    private bool $unsecuredUrls;

    public function __construct(
        ChannelContextInterface $channelContext,
        UrlHelper $urlHelper,
        bool $unsecuredUrls = false,
    ) {
        $this->channelContext = $channelContext;
        $this->urlHelper = $urlHelper;
        $this->unsecuredUrls = $unsecuredUrls;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('app_sylius_channel_url', [$this, 'generateChannelUrl']),
        ];
    }

    public function generateChannelUrl(string $path, ?ChannelInterface $channel = null): string
    {
        return $this->urlHelper->getAbsoluteUrl($path);
    }

}