<?php

namespace App\Repository\Channel;

use App\Entity\Channel\ChannelImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ChannelImage>
 *
 * @method ChannelImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChannelImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChannelImage[]    findAll()
 * @method ChannelImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChannelImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChannelImage::class);
    }

//    /**
//     * @return ChannelImage[] Returns an array of ChannelImage objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ChannelImage
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
