<?php

namespace App\Repository\Channel;

use App\Entity\Channel\Channelstock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

/**
 * @extends ServiceEntityRepository<Channelstock>
 *
 * @method Channelstock|null find($id, $lockMode = null, $lockVersion = null)
 * @method Channelstock|null findOneBy(array $criteria, array $orderBy = null)
 * @method Channelstock[]    findAll()
 * @method Channelstock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChannelStockRepository extends EntityRepository
{
    // public function __construct(ManagerRegistry $registry)
    // {
    //     parent::__construct($registry, Channelstock::class);
    // }

//    /**
//     * @return Channelstock[] Returns an array of Channelstock objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Channelstock
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
