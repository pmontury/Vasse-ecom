<?php

namespace App\Repository\Channel;

use App\Entity\Channel\ChannelPickUpSite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

/**
 * @extends ServiceEntityRepository<ChannelPickUpSite>
 *
 * @method ChannelPickUpSite|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChannelPickUpSite|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChannelPickUpSite[]    findAll()
 * @method ChannelPickUpSite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChannelPickUpSiteRepository extends EntityRepository
{
    // public function __construct(ManagerRegistry $registry)
    // {
    //     parent::__construct($registry, ChannelPickUpSite::class);
    // }

    //    /**
    //     * @return ChannelPickUpSite[] Returns an array of ChannelPickUpSite objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ChannelPickUpSite
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
