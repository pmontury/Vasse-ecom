<?php

declare(strict_types=1);

namespace App\Repository\Order;

use App\Model\Channel\ChannelInterface;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Repository\OrderRepositoryInterface as BaseOrderRepositoryInterface;

interface OrderRepositoryInterface extends BaseOrderRepositoryInterface
{
    public function findOneWithAddressByNumber(string $number): ?OrderInterface;

    public function countUnfulfilledForChannel(ChannelInterface $channel): int;

    public function createByCustomerQueryBuilder($customerId): QueryBuilder;

}