<?php

namespace App\Repository\Order;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use App\Model\Channel\ChannelInterface;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\OrderRepository as BaseOrderRepository;
use Sylius\Component\Core\Model\OrderInterface;
use Doctrine\ORM\QueryBuilder;

class OrderRepository extends BaseOrderRepository implements OrderRepositoryInterface
{
    private $entityManager;

    public function __construct(EntityManager $entityManager, ClassMetadata $class)
    {
        parent::__construct($entityManager, $class);

        $this->entityManager = $entityManager;
    }

    public function findOneWithAddressByNumber(string $number): ?OrderInterface
    {
        return $this->createQueryBuilder('o')
            ->addSelect('b')
            ->leftJoin('o.billingAddress', 'b')
            ->andWhere('o.number = :number')
            ->setParameter('number', $number)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function countUnfulfilledForChannel(ChannelInterface $channel): int 
    {
        $queryBuilder = $this->createQueryBuilder('o');
        $expr = $queryBuilder->expr();

        $query = $queryBuilder
            ->select('COUNT(o.id)')
            ->andWhere('o.channel = :channel')
            ->andWhere($expr->neq('o.state', ':state'))
            ->setParameter('channel', $channel)
            ->setParameter('state', OrderInterface::STATE_FULFILLED)
            ->getQuery();

        return (int) $query->getSingleScalarResult();
    }

    public function createByCustomerQueryBuilder($customerId): QueryBuilder
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.customer = :customerId')
            ->andWhere('o.state != :state')
            ->setParameter('customerId', $customerId)
            ->setParameter('state', OrderInterface::STATE_CART)
        ;
    }

}