<?php

declare(strict_types=1);

namespace App\Repository\Product;

use App\Model\Channel\ChannelInterface;
use Sylius\Component\Core\Repository\ProductRepositoryInterface as BaseProductRepositoryInterface;

interface ProductRepositoryInterface extends BaseProductRepositoryInterface
{
    public function findAllByChannel(ChannelInterface $channel);
}