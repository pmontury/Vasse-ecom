<?php

namespace App\Repository\Product;

use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductRepository as BaseProductRepository;
use App\Model\Channel\ChannelInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;

class ProductRepository extends BaseProductRepository implements ProductRepositoryInterface
// class ProductRepository extends BaseProductRepository
{
    private $entityManager;

    public function __construct(EntityManager $entityManager, ClassMetadata $class)
    {
        parent::__construct($entityManager, $class);

        $this->entityManager = $entityManager;
    }

    public function findAllByChannel(ChannelInterface $channel): array
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select('p.extranetProdId')
            ->from('App\Entity\Product\Product', 'p')
            ->andWhere(':channel MEMBER OF p.channels')
            ->andWhere('p.enabled = :enabled')
            ->addSelect('v AS variant')
            ->from('App\Entity\Product\ProductVariant', 'v')
            ->andWhere('v.product = p.id')
            ->setParameter('channel', $channel)
            ->setParameter('enabled', true)
        ;

        return $qb->getQuery()->getResult();
    }

//     $qb->select('p.extranetProdId')
//     ->from('App\Entity\Product\Product', 'p')
//     ->andWhere(':channel MEMBER OF p.channels')
//     ->andWhere('p.enabled = :enabled')
//     ->addSelect('v AS variant')
//     ->from('App\Entity\Product\ProductVariant', 'v')
//     ->andWhere('v.product = p.id')
//     ->setParameter('channel', $channel)
//     ->setParameter('enabled', true)
// ;

}