<?php

namespace App\Repository\Product;

use App\Entity\Product\ProductStock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class ProductStockRepository extends EntityRepository
{
    // public function __construct(ManagerRegistry $registry)
    // {
    //     parent::__construct($registry, ProductStock::class);
    // }

    public function findByExtranetProdId(int $extranetProdId)
    {
        $theQuery =  $this->createQueryBuilder('s')
            ->leftJoin('s.product', 'p', 'WITH', 's.product = p.id')
            ->where('p.extranetProdId = :extranetProdId')
            ->setParameter('extranetProdId', $extranetProdId)
            ->getQuery();
        // dd($theQuery);
        return $theQuery->getResult();
        // ;
    }





//    /**
//     * @return ProductStock[] Returns an array of ProductStock objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ProductStock
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
