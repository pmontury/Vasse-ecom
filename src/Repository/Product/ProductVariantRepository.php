<?php

declare(strict_types=1);

namespace App\Repository\Product;

use Sylius\AdminOrderCreationPlugin\Doctrine\ORM\ProductVariantRepositoryInterface;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductVariantRepository as BaseProductVariantRepository;

final class ProductVariantRepository extends BaseProductVariantRepository implements ProductVariantRepositoryInterface
{
    public function findByPhraseAndChannelCode(string $phrase, string $channelCode, string $locale): array
    {
        $expr = $this->getEntityManager()->getExpressionBuilder();

        return $this->createQueryBuilder('o')
            ->innerJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->innerJoin('o.product', 'p')
            ->innerJoin('p.channels', 'c')
            ->innerJoin('p.translations', 'pt', 'WITH', 'pt.locale = :locale')
            ->andWhere('c.code = :channel')
            ->andWhere($expr->orX('translation.name LIKE :phrase', 'o.code LIKE :phrase', 'pt.name LIKE :phrase'))
            ->setParameter('channel', $channelCode)
            ->setParameter('phrase', '%' . $phrase . '%')
            ->setParameter('locale', $locale)
            ->getQuery()
            ->getResult()
        ;
    }
}