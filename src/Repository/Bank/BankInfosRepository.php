<?php

namespace App\Repository\Bank;

use App\Entity\Bank\BankInfos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

/**
 * @extends ServiceEntityRepository<BankInfos>
 *
 * @method BankInfos|null find($id, $lockMode = null, $lockVersion = null)
 * @method BankInfos|null findOneBy(array $criteria, array $orderBy = null)
 * @method BankInfos[]    findAll()
 * @method BankInfos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankInfosRepository extends EntityRepository
{
    // public function __construct(ManagerRegistry $registry)
    // {
    //     parent::__construct($registry, BankInfos::class);
    // }

    //    /**
    //     * @return BankInfos[] Returns an array of BankInfos objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('b')
    //            ->andWhere('b.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('b.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?BankInfos
    //    {
    //        return $this->createQueryBuilder('b')
    //            ->andWhere('b.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
