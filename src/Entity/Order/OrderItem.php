<?php

declare(strict_types=1);

namespace App\Entity\Order;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Channel\ChannelStock;
use App\Model\Channel\ChannelStockInterface;
use App\Model\Order\OrderItemInterface;
use Sylius\Component\Core\Model\OrderItem as BaseOrderItem;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_order_item")
 */
#[ORM\Entity]
#[ORM\Table(name: 'sylius_order_item')]

class OrderItem extends BaseOrderItem implements OrderItemInterface
{
    // #[ORM\ManyToOne(inversedBy: 'orderItems')]
    // #[ORM\JoinColumn(nullable: false, name:'channel_stock_id')]
    // private ?ChannelStock $channelStock = null;

    // public function getChannelStock(): ?ChannelStock
    // {
    //     return $this->channelStock;
    // }

    // public function setChannelStock(?ChannelStockInterface $channelStock): void
    // {
    //     $this->channelStock = $channelStock;
    // }
}
