<?php

declare(strict_types=1);

namespace App\Entity\Order;

use App\Model\Order\OrderInterface;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Order as BaseOrder;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_order")
 */
#[ORM\Entity]
#[ORM\Table(name: 'sylius_order')]
class Order extends BaseOrder implements OrderInterface
{
    public function __construct()
    {
        parent::__construct();
        $this->hasAgreedToCGV = false;
    }

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $refClient = null;

    #[ORM\Column]
    private ?bool $hasAgreedToCGV = false;

    public function getRefClient(): ?string
    {
        return $this->refClient;
    }

    public function setRefClient(?string $refClient): static
    {
        $this->refClient = $refClient;

        return $this;
    }

    public function isHasAgreedToCGV(): ?bool
    {
        return $this->hasAgreedToCGV;
    }

    public function setHasAgreedToCGV(bool $hasAgreedToCGV): static
    {
        $this->hasAgreedToCGV = $hasAgreedToCGV;

        return $this;
    }
}
