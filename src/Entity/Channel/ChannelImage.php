<?php

namespace App\Entity\Channel;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Image;

#[ORM\Entity()]
#[ORM\Table(name: 'sylius_channel_image')]

class ChannelImage extends Image
{
}
