<?php

declare(strict_types=1);

namespace App\Entity\Channel;

use App\Entity\Bank\BankInfos;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Channel as BaseChannel;
use App\Model\Channel\ChannelInterface;
use DateTimeInterface;
use Sylius\Component\Core\Model\ImageAwareInterface;
use Sylius\Component\Core\Model\ImageInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_channel")
 */
#[ORM\Entity]
#[ORM\Table(name: 'sylius_channel')]

class Channel extends BaseChannel implements ChannelInterface, ImageAwareInterface
{
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $bannerContent = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $bannerLink = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $bannerLinkText = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?ChannelImage $image = null;

    #[ORM\Column]
    private ?bool $main = false;

    #[ORM\Column(nullable: true, unique: true)]
    private ?int $extranetChannelId = null;

    #[ORM\Column(length: 255)]
    private ?string $client = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $dateDebut = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $dateFin = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTime $deactivatedAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $saleClosedAt = null;

    #[ORM\ManyToOne]
    private ?BankInfos $bankInfos = null;

    #[ORM\ManyToOne(inversedBy: 'channels')]
    private ?ChannelPickUpSite $pickUpSite = null;

    public function getBannerContent(): ?string
    {
        return $this->bannerContent;
    }

    public function setBannerContent(?string $bannerContent): static
    {
        $this->bannerContent = $bannerContent;

        return $this;
    }

    public function getBannerLink(): ?string
    {
        return $this->bannerLink;
    }

    public function setBannerLink(?string $bannerLink): static
    {
        $this->bannerLink = $bannerLink;

        return $this;
    }

    public function getBannerLinkText(): ?string
    {
        return $this->bannerLinkText;
    }

    public function setBannerLinkText(?string $bannerLinkText): static
    {
        $this->bannerLinkText = $bannerLinkText;

        return $this;
    }

    public function getImage(): ?ImageInterface
    {
        return $this->image;
    }

    public function setImage(?ImageInterface $image): void
    {
        $this->image = $image;
    }

    public function isMain(): ?bool
    {
        return $this->main;
    }

    public function setMain(bool $main): void
    {
        $this->main = $main;
    }

    public function getExtranetChannelId(): ?int
    {
        return $this->extranetChannelId;
    }

    public function setExtranetChannelId(?int $extranetChannelId): void
    {
        $this->extranetChannelId = $extranetChannelId;
    }

    public function getClient(): ?string
    {
        return $this->client;
    }

    public function setClient(?string $client): void
    {
        $this->client = $client;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(?\DateTimeInterface $dateDebut): void
    {
        $this->dateDebut = $dateDebut;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): void
    {
        $this->dateFin = $dateFin;
    }

    public function getDeactivatedAt(): ?\DateTimeInterface
    {
        return $this->deactivatedAt;
    }

    public function setDeactivatedAt(?\DateTimeInterface $deactivatedAt): void
    {
        $this->deactivatedAt = $deactivatedAt;
    }

    public function getSaleClosedAt(): ?\DateTimeInterface
    {
        return $this->saleClosedAt;
    }

    public function setSaleClosedAt(?\DateTimeInterface $saleClosedAt): void
    {
        $this->saleClosedAt = $saleClosedAt;
    }

    public function getBankInfos(): ?BankInfos
    {
        return $this->bankInfos;
    }

    public function setBankInfos(?BankInfos $bankInfos): static
    {
        $this->bankInfos = $bankInfos;

        return $this;
    }

    public function getPickUpSite(): ?ChannelPickUpSite
    {
        return $this->pickUpSite;
    }

    public function setPickUpSite(?ChannelPickUpSite $pickUpSite): static
    {
        $this->pickUpSite = $pickUpSite;

        return $this;
    }


}
