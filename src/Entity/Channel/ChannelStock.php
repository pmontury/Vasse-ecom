<?php

namespace App\Entity\Channel;

use App\Entity\Order\OrderItem;
use App\Entity\Product\ProductVariant;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Model\Channel\ChannelStockInterface;
use App\Model\Product\ProductInterface;
use App\Model\Product\ProductVariantInterface;
use App\Repository\Channel\ChannelstockRepository;

#[ORM\Entity(repositoryClass: ChannelstockRepository::class)]
#[ORM\Table(name: 'sylius_channel_stock')]
#[ORM\Index(columns: ['product_variant_id', 'channel_code'], name: 'product_stock_channel_idx')]

class ChannelStock implements ChannelStockInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $channelId = null;

    #[ORM\Column]
    private ?int $onHand = null;

    #[ORM\Column]
    private ?int $onHold = null;

    #[ORM\ManyToOne(inversedBy: 'channelStocks')]
    #[ORM\JoinColumn(nullable: false, name:'product_variant_id')]
    private ?ProductVariant $productVariant = null;

    #[ORM\Column(nullable: true)]
    private ?int $extranetChannelId = null;

    #[ORM\Column(type: "integer")]
    #[ORM\Version]
    private ?int $version = 1;

    #[ORM\Column]
    private ?bool $tracked = true;

    #[ORM\Column(length: 255, name: 'channel_code')]
    private ?string $channelCode = null;

    #[ORM\OneToMany(mappedBy: 'channelStock', targetEntity: OrderItem::class)]
    private Collection $orderItems;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChannelId(): ?int
    {
        return $this->channelId;
    }

    public function setChannelId(int $channelId): static
    {
        $this->channelId = $channelId;

        return $this;
    }

    public function getOnHold(): ?int
    {
        return $this->onHold;
    }

    public function setOnHold(?int $onHold): void
    {
        $this->onHold = $onHold;
    }

    public function getOnHand(): ?int
    {
        return $this->onHand;
    }

    public function setOnHand(?int $onHand): void
    {
        $this->onHand = (0 > $onHand) ? 0 : $onHand;
    }

    public function getProductVariant(): ?ProductVariantInterface
    {
        return $this->productVariant;
    }

    public function setProductVariant(?ProductVariantInterface $productVariant): void
    {
        $this->productVariant = $productVariant;
    }

    public function getExtranetChannelId(): ?int
    {
        return $this->extranetChannelId;
    }

    public function setExtranetChannelId(?int $extranetChannelId): static
    {
        $this->extranetChannelId = $extranetChannelId;

        return $this;
    }

    public function getVersion(): ?int
    {
        return $this->version;
    }

    public function setVersion(int $version): static
    {
        $this->version = $version;

        return $this;
    }

    public function isTracked(): bool
    {
        return $this->tracked;
    }

    public function setTracked(bool $tracked): void
    {
        $this->tracked = $tracked;
    }

    public function getProduct(): ?ProductInterface
    {
        return $this->getProductVariant()->getProduct();
    }

    public function getInventoryName(): ?string
    {
        return $this->getProduct()->getName();
    }

    public function isInStock(): bool
    {
        return 0 < $this->onHand;
    }

    public function getChannelCode(): ?string
    {
        return $this->channelCode;
    }

    public function setChannelCode(string $channelCode): static
    {
        $this->channelCode = $channelCode;

        return $this;
    }

    /**
     * @return Collection<int, OrderItem>
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): static
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems->add($orderItem);
            $orderItem->setChannelStock($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): static
    {
        if ($this->orderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getChannelStock() === $this) {
                $orderItem->setChannelStock(null);
            }
        }

        return $this;
    }}
