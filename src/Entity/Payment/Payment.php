<?php

declare(strict_types=1);

namespace App\Entity\Payment;

use App\Model\Payment\PaymentInterface;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Payment as BasePayment;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_payment")
 */
#[ORM\Entity]
#[ORM\Table(name: 'sylius_payment')]

class Payment extends BasePayment implements PaymentInterface
{
    #[ORM\Column]
    private ?bool $isDeferred = false;

    public function isDeferred(): ?bool
    {
        return $this->isDeferred;
    }

    public function setIsDeferred(bool $isDeferred): static
    {
        $this->isDeferred = $isDeferred;

        return $this;
    }
}
