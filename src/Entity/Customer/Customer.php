<?php

declare(strict_types=1);

namespace App\Entity\Customer;

use App\Model\Customer\CustomerInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Customer as BaseCustomer;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_customer")
 */
#[ORM\Entity]
#[ORM\Table(name: 'sylius_customer')]

class Customer extends BaseCustomer implements CustomerInterface
{
    #[ORM\Column]
    private ?bool $allowedDeferredPayment = false;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $deferredPaymentDays = 0;

    // #[ORM\Column]
    // private ?bool $vasseAdmin = false;

    public function isAllowedDeferredPayment(): ?bool
    {
        return $this->allowedDeferredPayment;
    }

    public function setAllowedDeferredPayment(bool $allowedDeferredPayment): static
    {
        $this->allowedDeferredPayment = $allowedDeferredPayment;

        return $this;
    }

    public function getDeferredPaymentDays(): ?int
    {
        return $this->deferredPaymentDays;
    }

    public function setDeferredPaymentDays(int $deferredPaymentDays): static
    {
        $this->deferredPaymentDays = $deferredPaymentDays;

        return $this;
    }

    // public function isVasseAdmin(): ?bool
    // {
    //     return $this->vasseAdmin;
    // }

    // public function setVasseAdmin(bool $vasseAdmin): static
    // {
    //     $this->vasseAdmin = $vasseAdmin;

    //     return $this;
    // }
}
