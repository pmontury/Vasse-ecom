<?php

namespace App\Entity\Bank;

use App\Model\Bank\BankInfosInterface;
use App\Repository\Bank\BankInfosRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BankInfosRepository::class)]
#[ORM\Table(name: 'app_bank_infos')]

class BankInfos implements BankInfosInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $code = null;

    #[ORM\Column(length: 5)]
    private ?string $ribBanque = null;

    #[ORM\Column(length: 5)]
    private ?string $ribGuichet = null;

    #[ORM\Column(length: 11)]
    private ?string $ribCompte = null;

    #[ORM\Column(length: 2)]
    private ?string $ribCle = null;

    #[ORM\Column(length: 35)]
    private ?string $IBANEtranger = null;

    #[ORM\Column(length: 15)]
    private ?string $BIC = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getRibBanque(): ?string
    {
        return $this->ribBanque;
    }

    public function setRibBanque(string $ribBanque): static
    {
        $this->ribBanque = $ribBanque;

        return $this;
    }

    public function getRibGuichet(): ?string
    {
        return $this->ribGuichet;
    }

    public function setRibGuichet(string $ribGuichet): static
    {
        $this->ribGuichet = $ribGuichet;

        return $this;
    }

    public function getRibCompte(): ?string
    {
        return $this->ribCompte;
    }

    public function setRibCompte(string $ribCompte): static
    {
        $this->ribCompte = $ribCompte;

        return $this;
    }

    public function getRibCle(): ?string
    {
        return $this->ribCle;
    }

    public function setRibCle(string $ribCle): static
    {
        $this->ribCle = $ribCle;

        return $this;
    }

    public function getIBANEtranger(): ?string
    {
        return $this->IBANEtranger;
    }

    public function setIBANEtranger(string $IBANEtranger): static
    {
        $this->IBANEtranger = $IBANEtranger;

        return $this;
    }

    public function getBIC(): ?string
    {
        return $this->BIC;
    }

    public function setBIC(string $BIC): static
    {
        $this->BIC = $BIC;

        return $this;
    }
}
