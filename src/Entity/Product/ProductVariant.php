<?php

declare(strict_types=1);

namespace App\Entity\Product;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
// use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Core\Model\ProductVariant as BaseProductVariant;
use Sylius\Component\Product\Model\ProductVariantTranslationInterface;
use App\Entity\Channel\ChannelStock;
// use App\Model\Channel\ChannelStockInterface;
use App\Model\Product\ProductVariantInterface;
use App\Model\Channel\ChannelInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product_variant")
 */

#[ORM\Entity]
#[ORM\Table(name: 'sylius_product_variant')]

class ProductVariant extends BaseProductVariant implements ProductVariantInterface
{
    public function __construct()
    {
        parent::__construct();
    }

    #[ORM\Column(nullable: true)]
    private ?float $volume = null;

    protected function createTranslation(): ProductVariantTranslationInterface
    {
        return new ProductVariantTranslation();
    }

    public function getVolume(): ?float
    {
        return $this->volume;
    }

    public function setVolume(?float $volume): void
    {
        $this->volume = $volume;
    }

    public function getDescriptor(): string
    {
        return trim(sprintf('%s - %s (Quantité disponible %d)', $this->getProduct()->getName(), $this->getProduct()->getCode(), ($this->getOnHand() - $this->getOnHold())));
    }

}
