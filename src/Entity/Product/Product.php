<?php

declare(strict_types=1);

namespace App\Entity\Product;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Product as BaseProduct;
use App\Model\Product\ProductInterface;
use Sylius\Component\Product\Model\ProductTranslationInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product")
 */
#[ORM\Entity]
#[ORM\Table(name: 'sylius_product')]

class Product extends BaseProduct implements ProductInterface
{
    #[ORM\Column(length: 50, unique: true, nullable: true)]
    private ?string $internalReference = null;

    #[ORM\Column]
    private ?bool $new = true;

    #[ORM\Column(nullable: false, unique: true)]
    private ?int $extranetProdId = null;

    public function __construct()
    {
        parent::__construct();
    }

    protected function createTranslation(): ProductTranslationInterface
    {
        return new ProductTranslation();
    }

    public function getInternalReference(): ?string
    {
        return $this->internalReference;
    }

    public function setInternalReference(?string $internalReference): void
    {
        $this->internalReference = $internalReference;
    }

    public function isNew(): ?bool
    {
        return $this->new;
    }

    public function setNew(bool $new): void
    {
        $this->new = $new;
    }

    public function getExtranetProdId(): ?int
    {
        return $this->extranetProdId;
    }

    public function setExtranetProdId(int $extranetProdId): void
    {
        $this->extranetProdId = $extranetProdId;
    }

}
