<?php

declare(strict_types=1);

namespace App\Serializer;

use ApiPlatform\Problem\Serializer\ErrorNormalizerTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class HydraErrorNormalizer implements NormalizerInterface
{
    use ErrorNormalizerTrait;

    public const EXTRANET_ERROR = 999;

    private $debug;

    public function __construct(private NormalizerInterface $decorated, bool $debug = false)
    {
        $this->debug = $debug;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $errorData = json_decode($object->getMessage(), true);

        return $errorData ?? ['message' => $object->getMessage()];
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        if ($data->getCode() !== self::EXTRANET_ERROR || $this->debug) {
            return false;
        }
        
        return $this->decorated->supportsNormalization($data, $format);
    }
}