<?php

declare(strict_types=1);

namespace App\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Sylius\Component\Order\Model\OrderInterface;

final class ExtranetOrderNormalizer implements NormalizerInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'extranet_order_normalizer_already_called';

    public function __construct() {}

    public function normalize($object, $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED] = true;

        $data['commande'] = $object->getNumber();

        return $data;
    }

    public function supportsNormalization($data, $format = null, $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof OrderInterface && $this->isExtranetOperation($context);
    }

    private function isExtranetOperation(array $context): bool
    {
        if (isset($context['item_operation_name'])) {
            return \str_starts_with($context['item_operation_name'], 'extranet');
        }

        return false;
    }

}
