<?php

declare(strict_types=1);

namespace App\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use App\Model\Product\ProductInterface;

final class ExtranetProductNormalizer implements NormalizerInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'extranet_product_normalizer_already_called';

    public function __construct() {}

    public function normalize($object, $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED] = true;

        $data['prod_id'] = $object->getExtranetProdId();

        return $data;
    }

    public function supportsNormalization($data, $format = null, $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof ProductInterface && $this->isExtranetOperation($context);
    }

    private function isExtranetOperation(array $context): bool
    {
        if (isset($context['item_operation_name'])) {
            return \str_starts_with($context['item_operation_name'], 'extranet');
        }
        if (isset($context['collection_operation_name'])) {
            return \str_starts_with($context['collection_operation_name'], 'extranet');
        }

        return false;
    }

}
