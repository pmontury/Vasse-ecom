<?php
declare(strict_types=1);

namespace App\Serializer;

use ApiPlatform\OpenApi\OpenApi;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\DependencyInjection\Attribute\AsDecorator;

#[AsDecorator(decorates: 'api_platform.swagger.normalizer.documentation')]
final class ApiPlatformDocumentationNormalizer implements NormalizerInterface
{
    private NormalizerInterface $decorated;

    public function __construct(NormalizerInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    public function normalize($object, string $format = null, array $context = []): array
    {
        /** @var array $docs */
        $docs = $this->decorated->normalize($object, $format, $context);
        
        $path = '/api/v2/extranet/products/{extranetProdId}';
        $method = 'put';
        $parameters = $docs['paths'][$path][$method]['parameters'];
        $docs['paths'][$path][$method]['parameters'] = array_filter($parameters, function ($x) { return $x['name'] === 'extranetProdId'; });

        $path = '/api/v2/extranet/orders/{number}';
        $parameters = $docs['paths'][$path][$method]['parameters'];
        $docs['paths'][$path][$method]['parameters'] = array_filter($parameters, function ($x) { return $x['name'] === 'number'; });

        foreach ($docs['paths'] as $key => $path) {
            foreach ($path as $keyM => $method) {
                if (array_key_exists('parameters', $method)) {
                    $parameters = $docs['paths'][$key][$keyM]['parameters'];
                    $docs['paths'][$key][$keyM]['parameters'] = array_filter($parameters, function ($x) { return $x['name'] !== 'Accept-Language'; });
                }
            }
        }

        return $docs;
    }

}