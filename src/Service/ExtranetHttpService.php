<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\ExtranetTo\ExtranetLogin;
use App\Model\ExtranetTo\ExtranetRequestInterface;
use SM\Factory\FactoryInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

use Webmozart\Assert\Assert;

final class ExtranetHttpService
{
    private $authToken = null;

    public function __construct(private FactoryInterface $stateMachineFactory, 
                                private HttpClientInterface $vasseExtranetApi,
                                private ParameterBagInterface $params) {}

    public function sendNewOrder(ExtranetRequestInterface $newOrder): void
    {
        $this->sendRequest('POST', $this->params->get('extranet.route.add_commande'), $newOrder);
    }

    public function sendShippedOrder(ExtranetRequestInterface $shipment): void
    {
        $this->sendRequest('PUT', $this->params->get('extranet.route.commande_shipped'), $shipment);
    }

    public function sendEndSale(ExtranetRequestInterface $shipment): void
    {
        $this->sendRequest('POST', $this->params->get('extranet.route.end_sale'), $shipment);
    }

    private function sendRequest(string $method, string $route, ExtranetRequestInterface $msgData)
    {
        $maxRetries = 3;
        $nbRetries = 0;

        if (!$this->authToken) {
            $this->logToExtranet();
        }

        $done = false;

        do {
            $msgData->setToken($this->authToken);

            $response = $this->vasseExtranetApi->request($method, $route, ['body' => $msgData->getJson()]);

            $returnCode = $response->getStatusCode();

            $done = true;
            if ($returnCode === 200) {
                $done = true;
            } else {
                $nbRetries++;
                if ($nbRetries == $maxRetries) {
                    throw new HttpException($returnCode, $response->getInfo()['error'] ?$response->getInfo()['error'] :'');
                }

                switch ($returnCode) {
                    case 401:
                        $this->authToken = null;
                        $this->logToExtranet();
                        break;
                    default:
                        break;
                }
            }
        } while (!$done);
    }

    private function logToExtranet()
    {
        $this->authToken = null;

        $extranetLogin = new ExtranetLogin($this->params->get('extranet.log.email'), $this->params->get('extranet.log.pwd'));

        $response = $this->vasseExtranetApi->request(
            'POST',
            $this->params->get('extranet.route.auth'), 
            ['body' => $extranetLogin->getJson()]
        );

        $returnCode = $response->getStatusCode();

        if ($returnCode === 200) {
            $content = $response->toArray();
            $this->authToken = $content['token'];
        } else {
            switch ($returnCode) {
                case 401:
                    $this->authToken = null;
                    throw new HttpException(401, 'Invalid credentials');  //should not happen
                    break;
                default:
                    throw new HttpException($returnCode, $response->getInfo()['error'] ?$response->getInfo()['error'] :'');
                    break;
            }
        }
    }
}
