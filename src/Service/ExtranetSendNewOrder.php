<?php

namespace App\Service;

use SM\Factory\FactoryInterface;
use SM\StateMachine\StateMachineInterface;
use App\Service\ExtranetHttpService;
use App\Model\ExtranetTo\ExtranetNewOrder;
use App\Repository\Order\OrderRepositoryInterface;
use App\Sylius\Component\Core\OrderShippingStates;
use App\Sylius\Component\Core\OrderShippingTransitions;
use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Core\Model\OrderInterface as BaseOrderInterface;

final class ExtranetSendNewOrder
{
    public function __construct(private ExtranetHttpService $httpService,
                                private FactoryInterface $stateMachineFactory,
                                private EntityManagerInterface $em, 
                                private OrderRepositoryInterface $orderRepository) {}

    public function send(BaseOrderInterface $order): void
    {
        if ($order->getShippingState() == OrderShippingStates::STATE_AWAITING_PAYMENT) {
            $order = $this->orderRepository->findOneWithAddressByNumber($order->getNumber());

            $this->httpService->sendNewOrder(new ExtranetNewOrder($order));

            /** @var StateMachineInterface $stateMachine */
            $stateMachine = $this->stateMachineFactory->get($order, OrderShippingTransitions::GRAPH);

            if ($stateMachine->can(OrderShippingTransitions::TRANSITION_SHIPPING_START_PREPARATION)) {
                $stateMachine->apply(OrderShippingTransitions::TRANSITION_SHIPPING_START_PREPARATION);
            }
    
            $this->em->persist($order);
            $this->em->flush();
        }
    }

}
