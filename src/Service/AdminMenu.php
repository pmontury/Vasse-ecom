<?php

namespace App\Service;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Yaml\Yaml;
use \Knp\Menu\ItemInterface;

final class AdminMenu
{
    private $security;
    private $itemAuths;

    public function __construct($adminItemsAuth, Security $security) 
    {
        $this->security = $security;
        $this->itemAuths = Yaml::parseFile($adminItemsAuth);
    }

    public function filterItems(ItemInterface $menu): void
    {
        foreach ($menu->getChildren() as $child => $childInfos) {
            if (count($childInfos->getChildren()) > 0) {
                $this->filterItems($menu->getChild($child));

                if (count($childInfos->getChildren()) == 0) {
                    $menu->removeChild($child);
                }
    
            } else {
                // dump('- { path: "%sylius.security.admin_regex%' . substr($menu->getChild($child)->getUri(), strpos($menu->getChild($child)->getUri(), '/', 1)) . '", role: ROLE_ADMINISTRATION_ACCESS }');
                $childName = $menu->getChild($child)->getName();

                if (array_key_exists($childName, $this->itemAuths)) {
                    if (array_key_exists('enabled', $this->itemAuths[$childName]) && $this->itemAuths[$childName]['enabled'] === false) {
                        $menu->removeChild($child);
                    } else {
                        if (!$this->security->isGranted($this->itemAuths[$childName]['role'])) {
                            $menu->removeChild($child);
                        }
                    }
                } else {
                    $menu->removeChild($child);
                }
            }
        }
    }

}
