<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use App\Model\Product\ProductInterface;

final class ExtranetDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(private EntityManagerInterface $em, private Filesystem $filesystem, private ParameterBagInterface $params) {}

    public function supports($data, array $context = []): bool
    {
        if ($data instanceof ProductInterface) {
            return true;
        }

        return false;
    }
    
    public function persist($data, array $context = [])
    {
        $this->em->persist($data);
        $this->em->flush();

        if (isset($data->tempFile_path)) {
            foreach ($data->tempFile_path as $tempFile) {
                if ($this->filesystem->exists($tempFile)) {
                    $this->filesystem->remove($tempFile);
                }
            }
        }

        return $data;
    }
    
    public function remove($data, array $context = [])
    {
      // call your persistence layer to delete $data
    }

}