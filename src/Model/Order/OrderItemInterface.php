<?php

declare(strict_types=1);

namespace App\Model\Order;

use App\Model\Channel\ChannelStockInterface;
use Sylius\Component\Core\Model\OrderItemInterface as BaseOrderItemInterface;

interface OrderItemInterface extends BaseOrderItemInterface
{
    // public function getChannelStock(): ?ChannelStockInterface;

    // public function setChannelStock(?ChannelStockInterface $channelStock): void;

}