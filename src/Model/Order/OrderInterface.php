<?php

declare(strict_types=1);

namespace App\Model\Order;

use Sylius\Component\Core\Model\OrderInterface as BaseOrderInterface;

interface OrderInterface extends BaseOrderInterface
{
    public function getRefClient(): ?string;

    public function setRefClient(?string $refClient): static;

    public function isHasAgreedToCGV(): ?bool;

    public function setHasAgreedToCGV(bool $hasAgreedToCGV): static;
    
}