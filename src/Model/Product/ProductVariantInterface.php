<?php

declare(strict_types=1);

namespace App\Model\Product;

use App\Model\Channel\ChannelInterface;
use App\Model\Channel\ChannelStockInterface;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ProductVariantInterface as BaseProductVariantInterface;

interface ProductVariantInterface extends BaseProductVariantInterface
{
    public function getVolume(): ?float;

    public function setVolume(?float $volume): void;

}