<?php

declare(strict_types=1);

namespace App\Model\Product;

use Sylius\Component\Core\Model\ProductInterface as BaseProductInterface;

interface ProductInterface extends BaseProductInterface
{
    public function getInternalReference(): ?string;

    public function setInternalReference(?string $internalReference): void;

    public function isNew(): ?bool;

    public function setNew(bool $new): void;

    public function getExtranetProdId(): ?int;

    public function setExtranetProdId(int $extranetProdId): void;

}