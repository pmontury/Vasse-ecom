<?php

declare(strict_types=1);

namespace App\Model\Bank;

use Sylius\Component\Resource\Model\ResourceInterface;

interface BankInfosInterface extends ResourceInterface
{
    public function getId(): ?int;

    public function getCode(): ?string;

    public function setCode(?string $code): void;

    public function getRibBanque(): ?string;

    public function setRibBanque(string $ribBanque): static;

    public function getRibGuichet(): ?string;

    public function setRibGuichet(string $ribGuichet): static;

    public function getRibCompte(): ?string;

    public function setRibCompte(string $ribCompte): static;

    public function getRibCle(): ?string;

    public function setRibCle(string $ribCle): static;

    public function getIBANEtranger(): ?string;

    public function setIBANEtranger(string $IBANEtranger): static;

    public function getBIC(): ?string;

    public function setBIC(string $BIC): static;

}