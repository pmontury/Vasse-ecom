<?php

declare(strict_types=1);

namespace App\Model\Payment;

use  Sylius\Component\Core\Model\PaymentInterface as BasePaymentInterface;

interface PaymentInterface extends BasePaymentInterface
{
    public function isDeferred(): ?bool;

    public function setIsDeferred(bool $isDeferred): static;

}