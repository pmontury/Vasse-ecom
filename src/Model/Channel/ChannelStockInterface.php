<?php

declare(strict_types=1);

namespace App\Model\Channel;

use App\Model\Product\ProductVariantInterface;
use Sylius\Component\Inventory\Model\StockableInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

interface ChannelStockInterface extends StockableInterface, ResourceInterface
{
    public function getId(): ?int;

    public function getProductVariant(): ?ProductVariantInterface;

    public function setProductVariant(?ProductVariantInterface $productVariant): void;

    public function getChannelId(): ?int;

    public function setChannelId(int $channelId): static;

    public function getExtranetChannelId(): ?int;

    public function setExtranetChannelId(?int $extranetChannelId): static;

    public function getVersion(): ?int;

    public function setVersion(int $version): static;

    public function getChannelCode(): ?string;

    public function setChannelCode(string $channelCode): static;

}