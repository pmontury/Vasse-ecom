<?php

declare(strict_types=1);

namespace App\Model\Channel;

use App\Entity\Bank\BankInfos;
use App\Entity\Channel\ChannelPickUpSite;
use Sylius\Component\Core\Model\ChannelInterface as BaseChannelInterface;
use Sylius\Component\Core\Model\ImageInterface;

interface ChannelInterface extends BaseChannelInterface
{
    public function getBannerContent(): ?string;

    public function setBannerContent(?string $bannerContent): static;

    public function getBannerLink(): ?string;

    public function setBannerLink(?string $bannerLink): static;

    public function getBannerLinkText(): ?string;

    public function setBannerLinkText(?string $bannerLinkText): static;

    public function getImage(): ?ImageInterface;

    public function setImage(?ImageInterface $image): void;
    
    public function isMain(): ?bool;

    public function setMain(bool $main): void;

    public function setExtranetChannelId(?int $extranetChannelId): void;

    public function getExtranetChannelId(): ?int;

    public function setClient(?string $client): void;

    public function getClient(): ?string;

    public function setDateDebut(?\DateTimeInterface $dateDebut): void;

    public function getDateDebut(): ?\DateTimeInterface;

    public function setDateFin(?\DateTimeInterface $dateFin): void;

    public function getDateFin(): ?\DateTimeInterface;

    public function getDeactivatedAt(): ?\DateTimeInterface;

    public function setDeactivatedAt(?\DateTimeInterface $deactivatedAt): void;

    public function getSaleClosedAt(): ?\DateTimeInterface;

    public function setSaleClosedAt(?\DateTimeInterface $saleClosedAt): void;

    public function getPickUpSite(): ?ChannelPickUpSite;

    public function setPickUpSite(?ChannelPickUpSite $pickUpSite): static;

    public function getBankInfos(): ?BankInfos;

    public function setBankInfos(?BankInfos $bankInfos): static;

}