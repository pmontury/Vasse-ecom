<?php

declare(strict_types=1);

namespace App\Model\Channel;

use Sylius\Component\Resource\Model\ResourceInterface;

interface ChannelPickUpSiteInterface extends ResourceInterface
{
    public function getId(): ?int;

    public function getCompany(): ?string;

    public function setCompany(string $company): static;

    public function getAddress1(): ?string;

    public function setAddress1(string $address1): static;

    public function getAddress2(): ?string;

    public function setAddress2(?string $address2): static;

    public function getTown(): ?string;

    public function setTown(string $town): static;

    public function getZipCode(): ?string;

    public function setZipCode(string $zipCode): static;

    public function getTelephone(): ?string;

    public function setTelephone(?string $telephone): static;

}