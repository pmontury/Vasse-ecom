<?php

declare(strict_types = 1);

namespace App\Model\ExtranetFrom;

final class ExtranetProductModel implements ExtranetProductInterface
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $shortDescription;

    /** @var string */
    protected $description;

    /** @var string */
    protected $internalReference;

    /** @var string */
    protected $category;

    /** @var int */
    protected $stock;

    /** @var string */
    protected $brand;

    /** @var string */
    protected $color;

    /** @var double */
    protected $height;

    /** @var double */
    protected $width;

    /** @var double */
    protected $depth;

    /** @var double */
    protected $volume;

    /** @var array */
    protected $images;

    /** @var int */
    protected $extranetProdId;

    /** @var int */
    protected $extranetChannelId;

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setInternalReference(?string $internalReference): void
    {
        $this->internalReference = $internalReference;
    }

    public function getInternalReference(): ?string
    {
        return $this->internalReference;
    }

    public function setCategory(?string $category): void
    {
        $this->category = $category;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setStock(?int $stock): void
    {
        $this->stock = $stock;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setBrand(?string $brand): void
    {
        $this->brand = $brand;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setColor(?string $color): void
    {
        $this->color = $color;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setHeight(?float $height): void
    {
        $this->height = $height;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setWidth(?float $width): void
    {
        $this->width = $width;
    }

    public function getWidth(): ?float
    {
        return $this->width;
    }

    public function setDepth(?float $depth): void
    {
        $this->depth = $depth;
    }

    public function getDepth(): ?float
    {
        return $this->depth;
    }

    public function setVolume(?float $volume): void
    {
        $this->volume = $volume;
    }

    public function getVolume(): ?float
    {
        return $this->volume;
    }

    public function setImages(?array $images): void
    {
        $this->images = $images;
    }

    public function getImages(): ?array
    {
        return $this->images;
    }

    public function setExtranetProdId(?int $extranetProdId): void
    {
        $this->extranetProdId = $extranetProdId;
    }

    public function getExtranetProdId(): ?int
    {
        return $this->extranetProdId;
    }

    public function setExtranetChannelId(?int $extranetChannelId): void
    {
        $this->extranetChannelId = $extranetChannelId;
    }

    public function getExtranetChannelId(): ?int
    {
        return $this->extranetChannelId;
    }

}
