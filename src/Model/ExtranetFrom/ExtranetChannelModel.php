<?php

declare(strict_types = 1);

namespace App\Model\ExtranetFrom;

final class ExtranetChannelModel implements ExtranetChannelInterface
{
    /** @var int */
    protected $extranetChannelId;

    protected $client;

    /** @var string */
    protected $libelle;

    /** @var float */
    protected $dateDebut;

    /** @var float */
    protected $dateFin;

    
    public function setExtranetChannelId(?int $extranetChannelId): void
    {
        $this->extranetChannelId = $extranetChannelId;
    }

    public function getExtranetChannelId(): ?int
    {
        return $this->extranetChannelId;
    }

    public function setClient(?string $client): void
    {
        $this->client = $client;
    }

    public function getClient(): ?string
    {
        return $this->client;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setDateDebut(?float $dateDebut): void
    {
        $this->dateDebut = $dateDebut;
    }

    public function getDateDebut(): ?float
    {
        return $this->dateDebut;
    }

    public function setDateFin(?float $dateFin): void
    {
        $this->dateFin = $dateFin;
    }

    public function getDateFin(): ?float
    {
        return $this->dateFin;
    }
}
