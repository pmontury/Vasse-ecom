<?php

declare(strict_types = 1);

namespace App\Model\ExtranetFrom;

final class ExtranetProductOutputModel implements ExtranetProductOutputInterface
{
    /** @var int */
    protected $prod_id;

    public function getProdId(): ?int
    {
        return $this->prod_id;
    }

    public function setProdId(int $prodId): void
    {
        $this->prod_id = $prodId;
    }


}
