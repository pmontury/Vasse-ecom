<?php

declare(strict_types = 1);

namespace App\Model\ExtranetFrom;

final class ExtranetOrderModel implements ExtranetOrderInterface
{
    /** @var string */
    protected $state;

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): void
    {
        $this->state = $state;
    }

}
