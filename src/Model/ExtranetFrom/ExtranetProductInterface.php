<?php

declare(strict_types = 1);

namespace App\Model\ExtranetFrom;

interface ExtranetProductInterface
{
    public function setName(?string $name): void;

    public function getName(): ?string;

    public function setInternalReference(?string $internalReference): void;

    public function getInternalReference(): ?string;

    public function setCategory(?string $category): void;

    public function getCategory(): ?string;

    public function setStock(?int $stock): void;

    public function getStock(): ?int;

    public function setBrand(?string $brand): void;

    public function getBrand(): ?string;

    public function setColor(?string $color): void;

    public function getColor(): ?string;

    public function setHeight(?float $height): void;

    public function getHeight(): ?float;

    public function setVolume(?float $volume): void;

    public function getVolume(): ?float;

    public function setWidth(?float $width): void;

    public function getWidth(): ?float;

    public function setDepth(?float $depth): void;

    public function getDepth(): ?float;

    public function setImages(?array $images): void;

    public function getImages(): ?array;

    public function setExtranetProdId(?int $extranetProdId): void;

    public function getExtranetProdId(): ?int;

    public function setExtranetChannelId(?int $extranetChannelId): void;

    public function getExtranetChannelId(): ?int;

}
