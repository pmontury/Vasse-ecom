<?php

declare(strict_types = 1);

namespace App\Model\ExtranetFrom;

interface ExtranetOrderInterface
{
    public function getState(): ?string;

    public function setState(?string $state): void;
    
}
