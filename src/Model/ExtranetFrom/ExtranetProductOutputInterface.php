<?php

declare(strict_types=1);

namespace App\Model\ExtranetFrom;

use Sylius\Component\Core\Model\ProductInterface as BaseProductInterface;

interface ExtranetProductOutputInterface
{
    public function getProdId(): ?int;

    public function setProdId(int $prodId): void;
}