<?php

declare(strict_types = 1);

namespace App\Model\ExtranetFrom;

interface ExtranetChannelInterface
{
    public function setExtranetChannelId(?int $extranetChannelId): void;

    public function getExtranetChannelId(): ?int;

    public function setClient(?string $client): void;

    public function getClient(): ?string;

    public function setLibelle(?string $client): void;

    public function getLibelle(): ?string;

    public function setDateDebut(?float $dateDebut): void;

    public function getDateDebut(): ?float;

    public function setDateFin(?float $dateFin): void;

    public function getDateFin(): ?float;

}
