<?php

declare(strict_types=1);

namespace App\Model\Customer;

use Sylius\Component\Core\Model\CustomerInterface as BaseCustomerInterface;

interface CustomerInterface extends BaseCustomerInterface
{
    public function isAllowedDeferredPayment(): ?bool;

    public function setAllowedDeferredPayment(bool $allowedDeferredPayment): static;

    public function getDeferredPaymentDays(): ?int;

    public function setDeferredPaymentDays(int $deferredPaymentDays): static;
    
}