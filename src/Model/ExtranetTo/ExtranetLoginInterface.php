<?php

declare(strict_types = 1);

namespace App\Model\ExtranetTo;

interface ExtranetLoginInterface
{
    public function setEmail(string $ws_email): void;

    public function setPwd(string $ws_pwd): void;

    public function setDateAppel(float $ws_date_appel): void;

    public function getJson();
}
