<?php

declare(strict_types = 1);

namespace App\Model\ExtranetTo;

use DateTime;

interface ExtranetNewOrderInterface
{
    public function setCmdNumero(string $cmd_numero): void;

    public function setClientNom(string $client_nom): void;

    public function setClientPrenom(string $client_prenom): void;
    
    public function setDateCommande(DateTime $date_commande): void;

    public function setVentePrivee(int $vente_privee_id): void;

    public function setLstProduit($orderItems): void;

}
