<?php

declare(strict_types = 1);

namespace App\Model\ExtranetTo;

use DateTime;
use DateTimeImmutable;
use Sylius\Component\Core\Model\ShipmentInterface;

final class ExtranetShippedOrder implements ExtranetShippedOrderInterface, ExtranetRequestInterface
{
    public function __construct(ShipmentInterface $shipment) 
    {
        $now = new \DateTime("now", new \DateTimeZone('Europe/Paris'));

        $this->setDateAppel($now->getTimestamp() + $now->getOffset());
        $this->setCmdNumero($shipment->getOrder()->getNumber());
        $this->setShippingDate($shipment->getShippedAt());
    }

    /** @var string */
    protected $token;

    /** @var float */
    protected $date_appel;

    /** @var string */
    protected $cmd_numero;

    /** @var float */
    protected $date_expedition;

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function setDateAppel(float $date_appel): void
    {
        $this->date_appel = $date_appel;
    }

    public function setCmdNumero(string $cmd_numero): void
    {
        $this->cmd_numero = $cmd_numero;
    }

    public function setShippingDate(DateTimeImmutable $date_expedition): void
    {
        $this->date_expedition = $date_expedition->getTimestamp();
    }

    public function getJson()
    {
        return json_encode([
            'token' => $this->token,
            'date_appel' => $this->date_appel,
            'cmd_numero' => $this->cmd_numero,
            'date_expedition' => $this->date_expedition,
        ]);
    }
}
