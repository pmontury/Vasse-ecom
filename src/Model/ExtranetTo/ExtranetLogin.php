<?php

declare(strict_types = 1);

namespace App\Model\ExtranetTo;

final class ExtranetLogin implements ExtranetLoginInterface
{
    public function __construct(string $ws_email, string $ws_pwd) 
    {
        $now = new \DateTime();
        $this->setEmail($ws_email);
        $this->setPwd($ws_pwd);
        $this->setDateAppel($now->getTimestamp() + $now->getOffset());
    }

    /** @var string */
    protected $ws_email;

    /** @var string */
    protected $ws_pwd;

    /** @var float */
    protected $ws_date_appel;

    public function setEmail(string $ws_email): void
    {
        $this->ws_email = $ws_email;
    }

    public function setPwd(string $ws_pwd): void
    {
        $this->ws_pwd = $ws_pwd;
    }

    public function setDateAppel(float $ws_date_appel): void
    {
        $this->ws_date_appel = $ws_date_appel;
    }

    public function getJson()
    {
        return json_encode([
            'ws_email' => $this->ws_email,
            'ws_pwd' => $this->ws_pwd,
            'ws_date_appel' => $this->ws_date_appel
        ]);
    }
}
