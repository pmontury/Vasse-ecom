<?php

declare(strict_types = 1);

namespace App\Model\ExtranetTo;

use DateTimeImmutable;

interface ExtranetShippedOrderInterface
{
    public function setCmdNumero(string $cmd_numero): void;

    public function setShippingDate(DateTimeImmutable $date_expedition): void;

}
