<?php

declare(strict_types = 1);

namespace App\Model\ExtranetTo;

use DateTime;
use App\Model\Channel\ChannelInterface;

final class ExtranetEndSale implements ExtranetEndSaleInterface, ExtranetRequestInterface
{
    public function __construct($products, ChannelInterface $channel) 
    {
        $now = new \DateTime("now", new \DateTimeZone('Europe/Paris'));

        $this->setDateAppel($now->getTimestamp() + $now->getOffset());
        $this->setVentePriveeId($channel->getExtranetChannelId() ?? -1);
        $this->setLstProduit($products, $channel);
    }

    /** @var string */
    protected $token;

    /** @var float */
    protected $date_appel;

    /** @var int */
    protected $venteprivee_id;

    /** @var array */
    protected $lst_produit;

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function setDateAppel(float $date_appel): void
    {
        $this->date_appel = $date_appel;
    }

    public function setVentePriveeId(int $venteprivee_id): void
    {
        $this->venteprivee_id = $venteprivee_id;
    }

    public function setLstProduit($products, ChannelInterface $channel): void
    {
        foreach ($products as $product) {
            // $stock = $product['variant']->getStockForChannel($channel);

            $this->lst_produit[] = [
                'prod_id' => $product['extranetProdId'],
                'prod_qte' => $product['variant']->getOnHand()
            ];
        }
    }

    public function getJson()
    {
        return json_encode([
            'token' => $this->token,
            'date_appel' => $this->date_appel,
            'venteprivee_id' => $this->venteprivee_id,
            'lst_produit' => $this->lst_produit
        ]);
    }
}
