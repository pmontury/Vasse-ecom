<?php

declare(strict_types = 1);

namespace App\Model\ExtranetTo;

interface ExtranetRequestInterface
{
    public function setDateAppel(float $date_appel): void;

    public function setToken(string $token): void;

    public function getJson();
}
