<?php

declare(strict_types = 1);

namespace App\Model\ExtranetTo;

use App\Model\Channel\ChannelInterface;
use DateTime;

interface ExtranetEndSaleInterface
{
    public function setVentePriveeId(int $venteprivee_id): void;

    public function setLstProduit($products, ChannelInterface $channel): void;

}
