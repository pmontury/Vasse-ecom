<?php

declare(strict_types = 1);

namespace App\Model\ExtranetTo;

use DateTime;
use Sylius\Component\Core\Model\OrderInterface;
use App\Model\Channel\ChannelInterface;

final class ExtranetNewOrder implements ExtranetNewOrderInterface, ExtranetRequestInterface
{
    public function __construct(OrderInterface $order) 
    {
        $now = new \DateTime("now", new \DateTimeZone('Europe/Paris'));

        $this->setDateAppel($now->getTimestamp() + $now->getOffset());
        $this->setCmdNumero($order->getNumber());
        $this->setClientNom($order->getBillingAddress()->getLastName());
        $this->setClientPrenom($order->getBillingAddress()->getFirstName());
        $this->setDateCommande($order->getCreatedAt());

        /** @var ChannelInterface $channel */
        $channel = $order->getChannel();
        $this->setVentePrivee($channel->getExtranetChannelId() ?? -1);

        $this->setLstProduit($order->getItems());
    }

    /** @var string */
    protected $token;

    /** @var float */
    protected $date_appel;

    /** @var string */
    protected $cmd_numero;

    /** @var string */
    protected $client_nom;

    /** @var string */
    protected $client_prenom;

    /** @var float */
    protected $date_commande;

    /** @var int */
    protected $vente_privee_id;

    /** @var array */
    protected $lst_produit;

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function setDateAppel(float $date_appel): void
    {
        $this->date_appel = $date_appel;
    }

    public function setCmdNumero(string $cmd_numero): void
    {
        $this->cmd_numero = $cmd_numero;
    }

    public function setClientNom(string $client_nom): void
    {
        $this->client_nom = $client_nom;
    }

    public function setClientPrenom(string $client_prenom): void
    {
        $this->client_prenom = $client_prenom;
    }
    
    public function setDateCommande(DateTime $date_commande): void
    {
        $this->date_commande = $date_commande->getTimestamp();
    }

    public function setVentePrivee(int $vente_privee_id): void
    {
        $this->vente_privee_id = $vente_privee_id;
    }

    public function setLstProduit($orderItems): void
    {
        foreach ($orderItems as $item) {
            $this->lst_produit[] = [
                'prod_id' => $item->getVariant()->getProduct()->getExtranetProdId(),
                'prod_qte' => $item->getQuantity()
            ];
        }
    }

    public function getJson()
    {
        return json_encode([
            'token' => $this->token,
            'date_appel' => $this->date_appel,
            'cmd_numero' => $this->cmd_numero,
            'client_nom' => $this->client_nom,
            'client_prenom' => $this->client_prenom,
            'date_commande' => $this->date_commande,
            'venteprivee_id' => $this->vente_privee_id,
            'lst_produit' => $this->lst_produit
        ]);
    }
}
