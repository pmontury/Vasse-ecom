<?php

declare(strict_types=1);

namespace App\ContextProvider;

use Sylius\Bundle\UiBundle\ContextProvider\ContextProviderInterface;
use Sylius\Bundle\UiBundle\Registry\TemplateBlock;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Customer\Context\CustomerContextInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\DependencyInjection\Container;

final class MainChannelContextProvider implements ContextProviderInterface
{
    private $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function provide(array $templateContext, TemplateBlock $templateBlock): array
    {
        $mainChannel = $this->repository->findOneBy(['main' => 'true']);

        if (null === $mainChannel) {
            return $templateContext;
        }

        $templateContext['main_channel_id'] = $mainChannel->getId();

        return $templateContext;
    }

    public function supports(TemplateBlock $templateBlock): bool
    {
        return 'sylius.grid' === $templateBlock->getEventName()
                && 'content' === $templateBlock->getName();
    }
}