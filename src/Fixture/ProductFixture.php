<?php

declare(strict_types=1);

namespace App\Fixture;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Sylius\Bundle\CoreBundle\Fixture\ProductFixture as BaseProductFixture;

class ProductFixture extends BaseProductFixture
{
    protected function configureResourceNode(ArrayNodeDefinition $resourceNode): void
    {
        $resourceNode
            ->children()
                ->scalarNode('name')->cannotBeEmpty()->end()
                ->scalarNode('code')->cannotBeEmpty()->end()
                ->booleanNode('enabled')->end()
                ->booleanNode('tracked')->end()
                ->scalarNode('slug')->end()
                ->scalarNode('short_description')->cannotBeEmpty()->end()
                ->scalarNode('description')->cannotBeEmpty()->end()
                ->scalarNode('main_taxon')->cannotBeEmpty()->end()
                ->arrayNode('taxons')->scalarPrototype()->end()->end()
                ->variableNode('channels')
                    ->beforeNormalization()
                        ->ifNull()->thenUnset()
                    ->end()
                ->end()
                ->scalarNode('variant_selection_method')->end()
                ->arrayNode('product_attributes')->variablePrototype()->end()->end()
                ->arrayNode('product_options')->scalarPrototype()->end()->end()
                ->arrayNode('images')->variablePrototype()->end()->end()
                ->booleanNode('shipping_required')->end()
                ->scalarNode('tax_category')->end()
                ->booleanNode('new')->end()
                ->scalarNode('internal_reference')->cannotBeEmpty()->end()
                ->integerNode('extranet_prod_id')
        ;
    }
}
