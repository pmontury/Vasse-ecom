<?php

declare(strict_types=1);

namespace App\Fixture\Factory;

use App\Model\Bank\BankInfosInterface;
use Sylius\Bundle\CoreBundle\Fixture\Factory\AbstractExampleFactory;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sylius\Component\Resource\Factory\FactoryInterface;

class BankInfosFactory extends AbstractExampleFactory
{
    private OptionsResolver $optionsResolver;

    public function __construct(
        private FactoryInterface $bankInfosFactory,
        private RepositoryInterface $bankInfosRepository,
    ) {
        $this->optionsResolver = new OptionsResolver();

        $this->configureOptions($this->optionsResolver);
    }

    public function create(array $options = []): BankInfosInterface
    {
        $options = $this->optionsResolver->resolve($options);

        /** @var BankInfosInterface $bankInfos */
        $bankInfos = $this->bankInfosFactory->createNew();
        $bankInfos->setCode($options['code']);
        $bankInfos->setRibBanque($options['ribBanque']);
        $bankInfos->setRibGuichet($options['ribGuichet']);
        $bankInfos->setRibCompte($options['ribCompte']);
        $bankInfos->setRibCle($options['ribCle']);
        $bankInfos->setIBANEtranger($options['IBANEtranger']);
        $bankInfos->setBIC($options['BIC']);

        return $bankInfos;
    }

    protected function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault('code', '')
            ->setAllowedTypes('code', 'string')
            ->setDefault('ribBanque', '')
            ->setAllowedTypes('ribBanque', 'string')
            ->setDefault('ribGuichet', '')
            ->setAllowedTypes('ribGuichet', 'string')
            ->setDefault('ribCompte', '')
            ->setAllowedTypes('ribCompte', 'string')
            ->setDefault('ribCle', '')
            ->setAllowedTypes('ribCle', 'string')
            ->setDefault('IBANEtranger', '')
            ->setAllowedTypes('IBANEtranger', 'string')
            ->setDefault('BIC', '')
            ->setAllowedTypes('BIC', 'string')
        ;
    }
}
