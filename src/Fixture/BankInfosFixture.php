<?php

declare(strict_types=1);

namespace App\Fixture;

use Sylius\Bundle\CoreBundle\Fixture\AbstractResourceFixture;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class BankInfosFixture extends AbstractResourceFixture
{
    public function getName(): string
    {
        return 'bank_infos';
    }

    protected function configureResourceNode(ArrayNodeDefinition $resourceNode): void
    {
        $resourceNode
            ->children()
                ->scalarNode('code')->cannotBeEmpty()->end()
                ->scalarNode('ribBanque')->cannotBeEmpty()->end()
                ->scalarNode('ribGuichet')->cannotBeEmpty()->end()
                ->scalarNode('ribCompte')->cannotBeEmpty()->end()
                ->scalarNode('ribCle')->cannotBeEmpty()->end()
                ->scalarNode('IBANEtranger')->cannotBeEmpty()->end()
                ->scalarNode('BIC')->cannotBeEmpty()->end()
        ;
    }
}
