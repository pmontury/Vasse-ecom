<?php

declare(strict_types=1);

namespace App\Form\Extension;

use Sylius\Bundle\CoreBundle\Form\Type\Checkout\CompleteType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\IsTrue;

final class CompleteTypeExtension extends AbstractTypeExtension
{
    // private $security;

    // public function __construct(Security $security) 
    // {
    //     $this->security = $security;
    // }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void 
        {
            $form = $event->getForm();

            $form->add('refClient', TextType::class, [
                'required' => false,
                'label' => 'Ma référence commande',
            ]);
            $form->add('hasAgreedToCGV', CheckboxType::class, [
                'required' => true,
                'label' => "J'accepte les conditions générales de vente",
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter les conditions générales de vente',
                        'groups' => ['sylius_checkout_complete']
                    ])
                ]
            ]);
        });

    }

    public static function getExtendedTypes(): iterable
    {
        return [CompleteType::class];
    }
}