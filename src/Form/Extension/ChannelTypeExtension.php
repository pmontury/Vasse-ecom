<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Form\Type\ChannelImageType;
use App\Form\Type\ChannelPickUpSiteType;
use Sylius\Bundle\ChannelBundle\Form\Type\ChannelType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

final class ChannelTypeExtension extends AbstractTypeExtension
{
    private $security;

    public function __construct(Security $security, private RepositoryInterface $pickUpSitesRepository) 
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('bannerContent', TextType::class, [
                'required' => false,
                'label' => 'app.form.channel.banner_content',
            ])
            ->add('bannerLink', TextType::class, [
                'required' => false,
                'label' => 'app.form.channel.banner_link',
            ])
            ->add('bannerLinkText', TextType::class, [
                'required' => false,
                'label' => 'app.form.channel.banner_link_text',
            ])
            ->add('image', ChannelImageType::class, [
                'required' => false,
            ])
            ->add('pickUpSite', ChoiceType::class, [
                'required' => false,
                'label' => 'Site d\'enlèvement',
                'choices' => $this->pickUpSitesRepository->findAll(),
                'choice_value' => 'id',
                'choice_label' => 'descriptor',
                'empty_data' => null
            ])
            ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void 
        {
            $event->getForm()->remove('themeName');
            $event->getForm()->remove('locales');
            $event->getForm()->remove('defaultLocale');
            $event->getForm()->remove('menuTaxon');
            $event->getForm()->remove('skippingShippingStepAllowed');
            $event->getForm()->remove('skippingPaymentStepAllowed');
            $event->getForm()->remove('accountVerificationRequired');
            $event->getForm()->remove('baseCurrency');
            $event->getForm()->remove('currencies');
            $event->getForm()->remove('defaultTaxZone');
            $event->getForm()->remove('taxCalculationStrategy');
            $event->getForm()->remove('countries');
            if (!$this->security->isGranted('ROLE_SUPER_ADMINISTRATION_ACCESS')) {
                $event->getForm()->remove('hostname');
            }

        });
    }

    public static function getExtendedTypes(): iterable
    {
        return [ChannelType::class];
    }
}