<?php

declare(strict_types=1);

namespace App\Form\Extension;

use Sylius\Bundle\ProductBundle\Form\Type\ProductType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Bundle\SecurityBundle\Security;

final class ProductTypeExtension extends AbstractTypeExtension
{
    private $security;

    public function __construct(Security $security) 
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void 
        {
            if (!$this->security->isGranted('ROLE_SUPER_ADMINISTRATION_ACCESS')) {
                $event->getForm()->remove('attributes');
                $event->getForm()->remove('mainTaxon');
//                $event->getForm()->remove('associations');
                $event->getForm()->remove('productTaxons');
            }

            $event->getForm()->get('translations')->get('fr_FR')->remove('metaDescription');
            $event->getForm()->get('translations')->get('fr_FR')->remove('metaKeywords');
            $event->getForm()->remove('options');
            $event->getForm()->remove('variantSelectionMethod');
        });
    }

    public function getParent(): string
    {
        return ProductType::class;
    }

    public static function getExtendedTypes(): iterable
    {
        return [ProductType::class];
    }
}
