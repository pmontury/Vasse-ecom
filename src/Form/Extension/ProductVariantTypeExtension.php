<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Form\Type\ChannelStockType;
use App\Model\Channel\ChannelInterface;
use Sylius\Bundle\CoreBundle\Form\Type\ChannelCollectionType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductVariantType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\PositiveOrZero;

final class ProductVariantTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('tracked', CheckboxType::class, [
                'label' => 'sylius.form.variant.tracked',
                'disabled' => true
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void 
        {
            $productVariant = $event->getData();

            $event->getForm()->add('volume', NumberType::class, [
                'required' => false,
                'label' => 'sylius.form.variant.volume',
                'invalid_message' => 'sylius.product_variant.volume.invalid',
            ]);

            // $event->getForm()->add('channelStocks', ChannelCollectionType::class, [
            //     'entry_type' => ChannelStockType::class,
            //     'entry_options' => fn (ChannelInterface $channel) => [
            //         'channel' => $channel,
            //         'product_variant' => $productVariant,
            //         'required' => false,
            //         'constraints' => [new Valid(['groups' => ['sylius']])]
            //     ],
            // ]);

            $event->getForm()->add('onHand', IntegerType::class, [
                'label' => 'sylius.form.variant.on_hand',
                'attr' => [
                    'min' => 0,
                    'max' => $productVariant->getOnHand()
                ],
                'constraints' => [
                    new PositiveOrZero([
                        'message' => 'Vous ne pouvez pas entrer de valeur négative',
                        'groups' => ['sylius']
                    ]),
                    new LessThanOrEqual([
                        'value' => $productVariant->getOnHand(),
                        'message' => 'Vous ne pouvez pas incrémenter le stock',
                        'groups' => ['sylius']
                    ])
                ]
            ]);

            $event->getForm()->add('onHold', IntegerType::class, [
                'label' => 'sylius.form.variant.on_hold',
                'attr' => [
                    'readonly' => true
                ]
            ]);

            // $event->getForm()->add('shippingRequired', CheckboxType::class, [
            //     'label' => 'sylius.form.variant.shipping_required',
            //     'disabled' => true
            // ]);

            // $event->getForm()->remove('onHand');
            $event->getForm()->remove('weight');
            $event->getForm()->remove('taxCategory');
        });
    }

    public function getExtendedType(): string
    {
        return ProductVariantType::class;
    }

    public static function getExtendedTypes(): iterable
    {
        return [ProductVariantType::class];
    }
}
