<?php

declare(strict_types=1);

namespace App\Form\Extension;

use Sylius\Bundle\CustomerBundle\Form\Type\CustomerProfileType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Validator\Constraints\GreaterThan;

final class CustomerProfileTypeExtension extends AbstractTypeExtension
{
    private $security;

    public function __construct(Security $security) 
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('deferredPaymentDays', ChoiceType::class, [
                'label' => 'app.form.customer.deferred_payment_days',
                'choices' => [
                    'Aucun différé' => 0,
                    '30 jours' => 30,
                    '60 jours' => 60,
                    '90 jours' => 90
                ]
            ])
            // ->add('vasseAdmin', CheckboxType::class, [
            //     'required' => false,
            //     'label' => 'Administrateur Vasse',
            // ])
        ;

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event): void 
        {
            $data = $event->getData();

            if (!array_key_exists('deferredPaymentDays', $data)) {
                $event->getForm()->remove('deferredPaymentDays');
                return;
            }

            $data['allowedDeferredPayment'] = $data['deferredPaymentDays'] > 0 ? true : false;
            $event->getForm()->add('allowedDeferredPayment', CheckboxType::class);
            $event->setData(($data));
        });
    }

    public static function getExtendedTypes(): iterable
    {
        return [CustomerProfileType::class];
    }
}