<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Channel\ChannelImage;
use Sylius\Bundle\CoreBundle\Form\Type\ImageType;
use Symfony\Component\Form\FormBuilderInterface;

final class ChannelImageType extends ImageType
{
    public function __construct()
    {
        parent::__construct(ChannelImage::class, ['sylius']);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder->remove('type');
    }

    public function getBlockPrefix(): string
    {
        return 'sylius_channel_image';
    }
}