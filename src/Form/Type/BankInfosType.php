<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Bank\BankInfos;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class BankInfosType extends AbstractResourceType
{
    public function __construct()
    {
        parent::__construct(BankInfos::class, ['sylius']);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ribBanque', TextType::class, [
                'label' => 'Banque',
                'required' => true
            ])
            ->add('ribGuichet', TextType::class, [
                'label' => 'Guichet',
                'required' => true
            ])
            ->add('ribCompte', TextType::class, [
                'label' => 'N° de compte',
                'required' => true
            ])
            ->add('ribCle', TextType::class, [
                'label' => 'Clé',
                'required' => true
            ])
            ->add('IBANEtranger', TextType::class, [
                'label' => 'IBAN Etranger',
                'required' => true
            ])
            ->add('BIC', TextType::class, [
                'label' => 'BIC',
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BankInfos::class,
        ]);
    }
}