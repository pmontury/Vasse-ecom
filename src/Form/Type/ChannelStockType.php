<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Sylius Sp. z o.o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Channel\ChannelStock;
use App\Model\Channel\ChannelInterface;
use App\Model\Channel\ChannelStockInterface;
use App\Model\Product\ProductInterface;
use App\Model\Product\ProductVariantInterface;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\PositiveOrZero;

final class ChannelStockType extends AbstractResourceType
{
    public function __construct(private ?\Sylius\Component\Resource\Repository\RepositoryInterface $channelStockRepository = null) 
    {
        parent::__construct(ChannelStock::class, ['sylius']);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options): void 
        {
            $channelStock = $event->getData();

            $event->getForm()->add('version', HiddenType::class, [
                'attr' => [
                    'value' => $channelStock ?$channelStock->getVersion() :1
                ]
            ]);

            $event->getForm()->add('onHand', IntegerType::class, [
                'label' => 'sylius.form.stock.on_hand',
                'attr' => [
                    'min' => 0,
                    'max' => $channelStock ?$channelStock->getOnHand() :0,
                    'value' => $channelStock ?$channelStock->getOnHand() :0,
                    // 'readonly' => $productStock ?false :true
                ],
                'constraints' => [
                    new PositiveOrZero([
                        'message' => 'Vous ne pouvez pas entrer de valeur négative',
                        'groups' => ['sylius']
                    ]),
                    new LessThanOrEqual([
                        'value' => $channelStock ?$channelStock->getOnHand() :0,
                        'message' => 'Vous ne pouvez pas incrémenter le stock',
                        'groups' => ['sylius']
                    ])
                ]
            ]);

            $event->getForm()->add('onHold', IntegerType::class, [
                'label' => 'sylius.form.stock.on_hold',
                'attr' => [
                    'value' => $channelStock ?$channelStock->getOnHold() :0,
                    'readonly' => true,
                ],
            ]);

        });

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) use ($options): void 
        {
            $channelStock = $event->getData();

            if (!$channelStock instanceof $this->dataClass || !$channelStock instanceof ChannelStockInterface) {
                $event->setData(null);
                return;
            }

            if ($channelStock->getOnHand() === null) {
                $event->setData(null);
                if ($channelStock->getId() !== null) {
                    $this->channelStockRepository->remove($channelStock);
                }
                return;
            }

            $channelStock->setChannelId($options['channel']->getId());
            $channelStock->setChannelCode($options['channel']->getCode());
            $channelStock->setExtranetChannelId($options['channel']->getExtranetChannelId());
            $channelStock->setProductVariant($options['product_variant']);

            $event->setData($channelStock);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver
            ->setRequired('channel')
            ->setAllowedTypes('channel', [ChannelInterface::class])

            ->setDefined('product_variant')
            ->setAllowedTypes('product_variant', ['null', ProductVariantInterface::class])

            ->setDefaults([
                'label' => fn (Options $options): string => $options['channel']->getName(),
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'sylius_product_stock';
    }
}
