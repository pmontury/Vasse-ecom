Configuration serveur pour eviter les problèmes de droits au déploiement
=============================

***sur le serveur :***
1 - Ajouter septcinquante au groupe www-data
Cela permet à septcinquante d’avoir des permissions sur les fichiers appartenant au groupe www-data.

`sudo usermod -aG www-data septcinquante`

2 - Changer le groupe propriétaire des fichiers
Vous pouvez configurer les répertoires pour qu’ils appartiennent au groupe www-data tout en gardant le contrôle de l’utilisateur principal.

`sudo chgrp -R www-data /home/septcinquante/public_html/shared/public/media`

3 - Configurer les permissions du répertoire avec setgid
Le bit setgid sur un répertoire permet aux nouveaux fichiers ou sous-répertoires créés dans ce répertoire d’hériter du groupe du répertoire parent. Cela assure que tout fichier créé par www-data reste accessible pour le groupe.

`sudo chmod -R g+rw /home/septcinquante/public_html/shared/public/media`
`sudo chmod g+s /home/septcinquante/public_html/shared/public/media`

g+rw : Donne au groupe les permissions de lecture et d’écriture.
g+s : Active le setgid sur le répertoire.

4 - Créer une règle umask pour les nouveaux fichiers
Vous pouvez définir un umask dans la configuration Apache pour que les nouveaux fichiers créés aient les permissions correctes.

Ajoutez cette ligne dans la configuration Apache (généralement dans /etc/apache2/envvars) :

`umask 002`



Cela garantit que les fichiers créés par www-data sont accessibles en écriture par le groupe www-data.

6 - Redémarrer Apache
Si vous avez modifié la configuration d’Apache pour le umask, redémarrez le service :

`sudo systemctl restart apache2`

Pour la suppression des repertoire de cache lors du déploiement , j'ai aussi :
ajouté  au fichier bin/console du projet `umask 002`

Deploiement 
===============


avec DEPLOYER 
pour re7  vendor/bin/dep deploy_re7   -> première instalation et autre mise a jour du code 
puis eéxécuter vendor/bin/dep deploy:fixture:update -> première instalation pour les fixtures

Sur le serveur installer les taches cron du repertoire docker -> cron-> crontab 